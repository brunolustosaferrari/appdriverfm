AppDriverFm é um gerenciador financeiro simples para motoristas de aplicativo. 
O objetivo deste projeto é permitir o acompanhamento de ganhos e custos. Este
repositório contém a api para o aplicativo. Está sendo desenvolvida em Java, 
utilizando a framework Spring. As dependencias são gerenciadas pelo Maven.
Uma base de dados MySql é necessária para a execução do projeto; o script para
criação da base se encontra na raiz do repositório com o nome de 
criar_db_autenticacao.sql - a api utiliza uma arquitetura de entrega 
multi-tenant e as bases dos usuários são criadas em tempo de execução quando há
a criação da conta na plataforma. As conexão com a base deve ser configurada em
três lugares: no arquivo de configuração do spring em aplication.properties; na
classe ConstantesPredefinidas e na classe ConnectionFactory.