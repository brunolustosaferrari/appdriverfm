package br.com.bruno.appdriverfmapi.aplicativo;

import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.bruno.appdriverfmapi.AppdriverfmapiApplication;
import br.com.bruno.appdriverfmapi.autenticacao.AutenticacaoServicos;
import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioAutoridade;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppdriverfmapiApplication.class)
@WebAppConfiguration
public class AplicativoControllerTest {
	
	@Autowired
	private WebApplicationContext applicationContext;
	
	@Autowired
	private ObjectMapper mapper;
		
	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/snippets");
	
	private String authToken;
	private Aplicativo primeiroAplicativo;
	private MockMvc mvc;


	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(applicationContext).apply(documentationConfiguration(restDocumentation)).build();
		Usuario usuarioTeste = new Usuario().setIdUsuario(1L).setAutoridade(UsuarioAutoridade.MOTORISTA).setEmail("usuario@dominio.com.br");
		authToken =  AutenticacaoServicos.gerarToken(usuarioTeste);
		primeiroAplicativo = new Aplicativo()
									.setNome("Uber")
									.setDescricao("Descrição aqui");
	}

	@Test
	public void testAll() {
		this.testCriar();
		this.testListar();
		this.testBuscarPorId();
		this.testAtualizar();
		this.testExcluir();
	}
	

	private void testListar() {
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
									.get("/aplicativo")
									.header("Authorization", authToken))
									.andExpect(status().isOk())
									.andDo(document("listar",
											relaxedResponseFields(
													subsectionWithPath("_embedded").description("Vetor de aplicativos")
													)
											))
									.andReturn();
			System.out.println(resultado.getResponse().getContentAsString());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void testBuscarPorId() {
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.get("/aplicativo/{idAplicativo}", primeiroAplicativo.getIdAplicativo())
					.header("Authorization", authToken))
					.andExpect(status().isOk())
					.andDo(document("buscar-por-id",
							pathParameters(
									parameterWithName("idAplicativo").description("Id do aplicativo para busca")),
							relaxedResponseFields(
									subsectionWithPath("idAplicativo").description("Identificador do aplicativo"),
									subsectionWithPath("nome").description("Nome do aplicativo"),
									subsectionWithPath("descricao").description("Descrição")
									)
							))
					.andReturn();
			Aplicativo aplicativoRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Aplicativo.class);
			assertEquals(primeiroAplicativo.getNome(), aplicativoRetorno.getNome());
			assertEquals(primeiroAplicativo.getDescricao(), aplicativoRetorno.getDescricao());
			assertEquals(primeiroAplicativo.getIdAplicativo(), aplicativoRetorno.getIdAplicativo());
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void testCriar() {
		MvcResult resultado;
		try {
			resultado = mvc.perform(RestDocumentationRequestBuilders
					.post("/aplicativo")
					.contentType(MediaType.APPLICATION_JSON)
					.content(mapper.writeValueAsString(primeiroAplicativo))
					.header("Authorization", authToken))
					.andExpect(status().isCreated())
					.andDo(document("criar",
							relaxedRequestFields(
									fieldWithPath("nome").description("Nome do aplicativo - deve possuir no máximo 45 caracteres e não deve se repetir"),
									fieldWithPath("descricao").description("Campo para informações adicionais sobre o aplicativo - Opcional")
									)
							))
					.andReturn();
			
			Aplicativo aplicativoRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Aplicativo.class);
			assertEquals(primeiroAplicativo.getNome(), aplicativoRetorno.getNome());
			assertEquals(primeiroAplicativo.getDescricao(), aplicativoRetorno.getDescricao());
			primeiroAplicativo.setIdAplicativo(aplicativoRetorno.getIdAplicativo());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

	private void testAtualizar() {
		MvcResult resultado;
		primeiroAplicativo.setNome("Uber Select");
		try {
			resultado = mvc.perform(RestDocumentationRequestBuilders
					.put("/aplicativo")
					.contentType(MediaType.APPLICATION_JSON)
					.content(mapper.writeValueAsString(primeiroAplicativo))
					.header("Authorization", authToken))
					.andExpect(status().isAccepted())
					.andDo(document("atualizar",
							relaxedRequestFields(
									fieldWithPath("idAplicativo").description("Identificador do aplicativo para atualização"),
									fieldWithPath("nome").description("Nome do aplicativo - deve possuir no máximo 45 caracteres e não deve se repetir"),
									fieldWithPath("descricao").description("Campo para informações adicionais sobre o aplicativo - Opcional")
									)
							))
					.andReturn();
			
			Aplicativo aplicativoRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Aplicativo.class);
			assertEquals(primeiroAplicativo.getNome(), aplicativoRetorno.getNome());
			assertEquals(primeiroAplicativo.getDescricao(), aplicativoRetorno.getDescricao());
			primeiroAplicativo.setIdAplicativo(aplicativoRetorno.getIdAplicativo());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void testExcluir() {
		try {
			mvc.perform(RestDocumentationRequestBuilders
					.delete("/aplicativo/{idAplicativo}", primeiroAplicativo.getIdAplicativo())
					.header("Authorization", authToken))
					.andExpect(status().isNoContent())
					.andDo(document("excluir",
							pathParameters(
									parameterWithName("idAplicativo").description("Id do aplicativo para busca")
									)
							));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
	}

}
