package br.com.bruno.appdriverfmapi.custo;

import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.relaxedRequestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.relaxedResponseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.bruno.appdriverfmapi.AppdriverfmapiApplication;
import br.com.bruno.appdriverfmapi.autenticacao.AutenticacaoServicos;
import br.com.bruno.appdriverfmapi.multiTenancy.ContextoInquilino;
import br.com.bruno.appdriverfmapi.parcela.Parcela;
import br.com.bruno.appdriverfmapi.parcela.ParcelaRepository;
import br.com.bruno.appdriverfmapi.tipocusto.TipoCusto;
import br.com.bruno.appdriverfmapi.tipocusto.TipoCustoRepository;
import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioRepository;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;
import br.com.bruno.appdriverfmapi.veiculo.Veiculo;
import br.com.bruno.appdriverfmapi.veiculo.VeiculoRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppdriverfmapiApplication.class)
@WebAppConfiguration
public class CustoControllerTest {
	
	@Autowired
	private WebApplicationContext applicationContext;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	UsuarioRepository repositorioUsuario;
	
	@Autowired
	private TipoCustoRepository repositorioTipo;
	
	@Autowired
	private CustoRepository repositorio;
	
	@Autowired 
	private VeiculoRepository repositorioVeiculo;
	
	@Autowired
	private ParcelaRepository repositorioParcela;
		
	private String authToken;
	private MockMvc mvc;
	private Usuario usuarioTeste;
	private TipoCusto tipoCustoTeste;
	private Custo custoTeste;
	private Parcela parcelaTeste;
	private Veiculo veiculoTeste;
	
	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/snippets");

	@Before
	public void setUp() throws Exception {
		
		mvc = MockMvcBuilders.webAppContextSetup(applicationContext).apply(documentationConfiguration(restDocumentation)).build();
		usuarioTeste = repositorioUsuario.findByEmail("testebase@teste.com");
		authToken = AutenticacaoServicos.gerarToken(usuarioTeste);
		tipoCustoTeste = new TipoCusto().setDescricao("Tipo de custo teste");
		veiculoTeste = new Veiculo().setDescricao("Descrição aqui").setNome("Nome para referência").setPlaca("AAA-9999");
		
		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		tipoCustoTeste = repositorioTipo.save(tipoCustoTeste);
		repositorioVeiculo.save(veiculoTeste);
		ContextoInquilino.limpar();
		
		
		custoTeste = new Custo()
				.setTitulo("Titulo do Custo")
				.setDescricao("Descricao aqui")
				.setData(Calendar.getInstance())
				.setTipo(tipoCustoTeste)
				.setVeiculo(veiculoTeste);
		
		parcelaTeste = new Parcela()
				.setDataVencimento(Calendar.getInstance())
				.setValor(new BigDecimal(125.75));
		
		custoTeste.addParcela(parcelaTeste);
		
	}
		
	

	@After
	public void tearDown() throws Exception {
		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		repositorioTipo.delete(tipoCustoTeste);
		repositorioVeiculo.delete(veiculoTeste);
		ContextoInquilino.limpar();
	}

	@Test
	public void testBuscarPorId() {
		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		custoTeste = repositorio.save(custoTeste);
		custoTeste.getParcelas().forEach(parcela->repositorioParcela.save(parcela.setCusto(custoTeste)));
		ContextoInquilino.limpar();
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.get("/custo/{idCusto}", custoTeste.getIdCusto())
					.header("Authorization", authToken))
					.andExpect(status().isOk())
					.andDo(document("buscar-por-id",
							pathParameters(
									parameterWithName("idCusto").description("Indentificador do custo. Obrigatório.")),
							relaxedResponseFields(
									subsectionWithPath("idCusto").description("Indentificador do custo."),
									subsectionWithPath("tipo").description("Tipo de custo."),
									subsectionWithPath("valor").description("Valor pago no total das parcelas."),
									subsectionWithPath("data").description("Data de ocorrência do custo. Formato ISO para data yyyy-MM-dd. "),
									subsectionWithPath("titulo").description("Título do custo."),
									subsectionWithPath("descricao").description("Descrição do custo."),
									subsectionWithPath("parcelas[]").description("Parcelas de pagamento"),
									subsectionWithPath("parcelas[].valor").description("Valor pago na parcela"),
									subsectionWithPath("parcelas[].dataVencimento").description("Data de vencimento da parcela."),
									subsectionWithPath("veiculo").description("Veículo para o qual o gasto será rateado")
									)
							))
					.andReturn();
			
			Custo custoRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Custo.class);
			System.out.println("Custo retorno:" + mapper.writeValueAsString(custoRetorno));
			assertEquals(custoTeste.getDescricao(), custoRetorno.getDescricao());
			ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
			repositorio.delete(custoRetorno);
			ContextoInquilino.limpar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testCriar() {
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.post("/custo")
					.contentType(MediaType.APPLICATION_JSON)
					.content(mapper.writeValueAsString(custoTeste))
					.header("Authorization", authToken))
					.andExpect(status().isCreated())
					.andDo(document("criar",
							relaxedRequestFields(
									fieldWithPath("tipo").description("Tipo de custo. Obrigatório. "),
									fieldWithPath("data").description("Data de ocorrência do custo. Formato ISO para data yyyy-MM-dd. "),
									fieldWithPath("titulo").description("Título do custo. Máximo de 45 caracteres. "),
									fieldWithPath("descricao").description("Descrição do custo."),
									fieldWithPath("parcelas[]").description("Parcelas de pagamento"),
									fieldWithPath("parcelas[].valor").description("Valor pago na parcela - obrigatório."),
									fieldWithPath("parcelas[].dataVencimento").description("Data de vencimento da parcela. Formato ISO para data yyyy-MM-dd. "),
									fieldWithPath("veiculo").description("Veículo para o qual o gasto será rateado")
									)
							))
					.andReturn();
			
			System.out.println(resultado.getResponse().getContentAsString());
			Custo custoRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Custo.class);
			assertEquals(custoTeste.getDescricao(), custoRetorno.getDescricao());
			ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
			repositorio.delete(custoRetorno);
			ContextoInquilino.limpar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testAtualizar() {
		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		custoTeste = repositorio.save(custoTeste);
		custoTeste.setDescricao("Nova descrição atualizada");
		ContextoInquilino.limpar();
		
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.put("/custo")
					.content(mapper.writeValueAsString(custoTeste))
					.contentType(MediaType.APPLICATION_JSON)
					.header("Authorization", authToken))
					.andExpect(status().isAccepted())
					.andDo(document("atualizar",
							relaxedRequestFields(
									fieldWithPath("idCusto").description("Indentificador do custo. Obrigatório. "),
									fieldWithPath("tipo").description("Tipo de custo. Obrigatório. "),
									fieldWithPath("data").description("Data de ocorrência do custo. Formato ISO para data yyyy-MM-dd. "),
									fieldWithPath("titulo").description("Título do custo. Máximo de 45 caracteres. "),
									fieldWithPath("descricao").description("Descrição do custo."),
									fieldWithPath("parcelas[]").description("Parcelas de pagamento"),
									fieldWithPath("parcelas[].valor").description("Valor pago na parcela - obrigatório."),
									fieldWithPath("parcelas[].dataVencimento").description("Data de vencimento da parcela. Formato ISO para data yyyy-MM-dd. "),
									fieldWithPath("veiculo").description("Veículo para o qual o gasto será rateado")
							)))
					.andReturn();
			
			System.out.println(resultado.getResponse().getContentAsString());
			Custo custoRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Custo.class);
			assertEquals(custoTeste.getDescricao(), custoRetorno.getDescricao());
			ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
			repositorio.delete(custoRetorno);
			ContextoInquilino.limpar();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testExcluir() {
		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		custoTeste = repositorio.save(custoTeste);
		ContextoInquilino.limpar();
		
		try {
			mvc.perform(RestDocumentationRequestBuilders
					.delete("/custo/{idCusto}", custoTeste.getIdCusto())
					.header("Authorization", authToken))
					.andExpect(status().isNoContent())
					.andDo(document("excluir",
							pathParameters(
									parameterWithName("idCusto").description("Indentificador do custo. Obrigatório. "))
									)
							);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
