package br.com.bruno.appdriverfmapi.autenticacao;

import org.junit.Before;
import org.junit.Test;

import com.auth0.jwt.interfaces.DecodedJWT;

import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioAutoridade;
import junit.framework.TestCase;

public class AutenticacaoServicosTest extends TestCase {

	protected Usuario usuarioTeste;
	protected String token;
	
	@Before
	public void setUp() throws Exception {
		usuarioTeste = new Usuario()
				.setIdUsuario(1L)
				.setEmail("teste@teste.com.br")
				.setSenha("123456")
				.setAutoridade(UsuarioAutoridade.MOTORISTA);
				
	}

	@Test
	public void testGerarValidarToken() {
		
		token = AutenticacaoServicos.gerarToken(usuarioTeste);
		assertNotNull(token);
		
		DecodedJWT tokenDecodificado = AutenticacaoServicos.validarToken(token);
		assertEquals(usuarioTeste.getEmail(), tokenDecodificado.getSubject());
		assertEquals(usuarioTeste.getAutoridade().toString(), tokenDecodificado.getClaim("autoridade").asString());
		assertEquals( usuarioTeste.getIdUsuario().longValue(), Long.parseLong( tokenDecodificado.getClaim("idUsuario").asString() ) );
	}


}
