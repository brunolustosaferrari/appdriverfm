package br.com.bruno.appdriverfmapi.autenticacao;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;


import java.security.NoSuchAlgorithmException;

import javax.transaction.Transactional;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import br.com.bruno.appdriverfmapi.AppdriverfmapiApplication;
import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioAutoridade;
import br.com.bruno.appdriverfmapi.usuario.UsuarioRepository;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;
import junit.framework.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppdriverfmapiApplication.class)
@WebAppConfiguration
@Transactional
public class AutenticacaoControllerTest extends TestCase {
	
	MockMvc mvc;
	Usuario usuarioTeste;
	
	@Autowired
	UsuarioRepository repositorio;
	
	@Autowired
	WebApplicationContext applicationContext;
	
	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/snippets");
	
	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(applicationContext).apply(documentationConfiguration(restDocumentation)).build();
		usuarioTeste = new Usuario().setEmail("teste@teste.com.br").setSenha(Utilitario.gerarHash256("123456")).setAutoridade(UsuarioAutoridade.MOTORISTA);
		repositorio.save(usuarioTeste);
	
	}

	@After
	public void tearDown() throws Exception {
		repositorio.delete(usuarioTeste);
	}

	@Test
	public void testAutenticar() throws NoSuchAlgorithmException, Exception {
		MvcResult resultado = mvc.perform( RestDocumentationRequestBuilders.get("/autenticacao/{email}/{senha}", usuarioTeste.getEmail(), "123456" ) )
				.andExpect(status().isAccepted())
				.andDo(document("autenticacao",
						pathParameters(
								parameterWithName("email").description("Email do Usuário"),
								parameterWithName("senha").description("Senha do Usuario")
								)
						))
				.andReturn();
		System.out.println(resultado.getResponse().getContentAsString());		
	}

}
