package br.com.bruno.appdriverfmapi.autenticacao;

import static org.junit.Assert.*;

import javax.servlet.http.Cookie;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import br.com.bruno.appdriverfmapi.AppdriverfmapiApplication;
import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioAutoridade;
import br.com.bruno.appdriverfmapi.usuario.UsuarioRepository;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppdriverfmapiApplication.class)
@WebAppConfiguration
public class AutenticacaoInterceptorTest {

	@Autowired
	private RequestMappingHandlerAdapter handlerAdapter;

	@Autowired
	private RequestMappingHandlerMapping handlerMapping;
	
	@Autowired
	UsuarioRepository repositorio;
	
	@Autowired
	WebApplicationContext applicationContext;
	
	
	private Usuario usuarioRequest;
	private Cookie cookieToken;
	
	
	@Before
	public void setUp() throws Exception {
		
		usuarioRequest = new Usuario().setEmail("teste2@teste.com.br").setSenha(Utilitario.gerarHash256("123456")).setAutoridade(UsuarioAutoridade.MOTORISTA);
		repositorio.save(usuarioRequest);
		
		cookieToken = new Cookie("tokenAutenticacao", AutenticacaoServicos.gerarToken(usuarioRequest));
		
	}

	@After
	public void tearDown() throws Exception {
		repositorio.delete(usuarioRequest);
	}

	@Test
	public void testPreHandle() throws Exception {

		 MockHttpServletRequest request;
		 MockHttpServletResponse response;
		 HandlerExecutionChain handlerExecutionChain;
		 HandlerInterceptor[] interceptors;
		
		request = new MockHttpServletRequest();
		request.setCookies(cookieToken);
		request.setRequestURI( "/usuario/" + usuarioRequest.getIdUsuario().toString() );
	    request.setMethod("GET");
	    
		response = new MockHttpServletResponse();
		handlerExecutionChain = handlerMapping.getHandler(request);
		interceptors = handlerExecutionChain.getInterceptors();
	    
	    for(HandlerInterceptor interceptor : interceptors){
	        interceptor.preHandle(request, response, handlerExecutionChain.getHandler());
	    }

	    ModelAndView mav = handlerAdapter. handle(request, response, handlerExecutionChain.getHandler());

	    for(HandlerInterceptor interceptor : interceptors){
	        interceptor.postHandle(request, response, handlerExecutionChain.getHandler(), mav);
	    }
	    
	    assertEquals(200, response.getStatus());
	}
	
	@Test
	public void testPreHandleFail() throws Exception {
		
		 MockHttpServletRequest request;
		 MockHttpServletResponse response;
		 HandlerExecutionChain handlerExecutionChain;
		 HandlerInterceptor[] interceptors;
		
		request = new MockHttpServletRequest();
		request.setCookies(cookieToken);
		request.setRequestURI( "/usuario/");
	    request.setMethod("GET");

		response = new MockHttpServletResponse();
		handlerExecutionChain = handlerMapping.getHandler(request);
		interceptors = handlerExecutionChain.getInterceptors();
	    
	    for(HandlerInterceptor interceptor : interceptors){
	        interceptor.preHandle(request, response, handlerExecutionChain.getHandler());
	    }

	    ModelAndView mav = handlerAdapter. handle(request, response, handlerExecutionChain.getHandler());

	    for(HandlerInterceptor interceptor : interceptors){
	        interceptor.postHandle(request, response, handlerExecutionChain.getHandler(), mav);
	    }

	    assertEquals(405, response.getStatus());
	}

}
