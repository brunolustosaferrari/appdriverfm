package br.com.bruno.appdriverfmapi.autenticacao;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import junit.framework.Test;
import junit.framework.TestSuite;

@RunWith(Suite.class)
@SuiteClasses({ AutenticacaoControllerTest.class, AutenticacaoServicosTest.class })
public class AllTests {
	
	public static Test suite() {
		
		TestSuite suite = new TestSuite("Teste para autenticação");
		suite.addTestSuite(AutenticacaoServicosTest.class);
		suite.addTestSuite(AutenticacaoControllerTest.class);
		return suite;
	}
	
}
