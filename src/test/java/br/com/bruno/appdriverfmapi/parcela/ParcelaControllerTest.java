package br.com.bruno.appdriverfmapi.parcela;

import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.relaxedRequestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.relaxedResponseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.Calendar;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.bruno.appdriverfmapi.AppdriverfmapiApplication;
import br.com.bruno.appdriverfmapi.autenticacao.AutenticacaoServicos;
import br.com.bruno.appdriverfmapi.custo.Custo;
import br.com.bruno.appdriverfmapi.custo.CustoRepository;
import br.com.bruno.appdriverfmapi.multiTenancy.ContextoInquilino;
import br.com.bruno.appdriverfmapi.tipocusto.TipoCusto;
import br.com.bruno.appdriverfmapi.tipocusto.TipoCustoRepository;
import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioRepository;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;
import br.com.bruno.appdriverfmapi.veiculo.Veiculo;
import br.com.bruno.appdriverfmapi.veiculo.VeiculoRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppdriverfmapiApplication.class)
@WebAppConfiguration
public class ParcelaControllerTest {
	
	@Autowired
	private WebApplicationContext applicationContext;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	UsuarioRepository repositorioUsuario;
	
	@Autowired
	private ParcelaRepository repositorio;
	
	@Autowired
	private CustoRepository repositorioCusto;
	
	@Autowired
	private TipoCustoRepository repositorioTipo;
	
	@Autowired 
	private VeiculoRepository repositorioVeiculo;
	
	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/snippets");
	
	private String authToken;
	private MockMvc mvc;
	private Usuario usuarioTeste;
	private Parcela parcelaTeste;
	private Custo custoTeste;
	private TipoCusto tipoCustoTeste;
	private Veiculo veiculoTeste;

	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(applicationContext).apply(documentationConfiguration(restDocumentation)).build();
		usuarioTeste = repositorioUsuario.findByEmail("testebase@teste.com");
		authToken = AutenticacaoServicos.gerarToken(usuarioTeste);
		
		parcelaTeste = new Parcela().setValor(new BigDecimal(100)).setDataVencimento(Calendar.getInstance());
		tipoCustoTeste = new TipoCusto().setDescricao("Tipo de custo teste");
		veiculoTeste = new Veiculo().setDescricao("Descrição aqui").setNome("Nome para referência").setPlaca("AAA-9999");
		custoTeste = new Custo().setTitulo("Teste").setTipo(tipoCustoTeste).setVeiculo(veiculoTeste);
		
		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		repositorioVeiculo.save(veiculoTeste);
		repositorioTipo.save(tipoCustoTeste);
		repositorioCusto.save(custoTeste);
		custoTeste.addParcela(parcelaTeste);
		parcelaTeste = repositorio.save(parcelaTeste);
		ContextoInquilino.limpar();
	}

	@After
	public void tearDown() throws Exception {
		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		repositorio.delete(parcelaTeste);
		custoTeste.removeParcela(parcelaTeste);
		repositorioCusto.delete(custoTeste);
		repositorioVeiculo.delete(veiculoTeste);
		repositorioTipo.delete(tipoCustoTeste);
		ContextoInquilino.limpar();
	}

	@Test
	public void testBuscar() {
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.get("/parcela/{idParcela}", parcelaTeste.getIdParcela())
					.header("Authorization", authToken))
					.andExpect(status().isOk())
					.andDo(document("buscar-por-id",
							pathParameters(
									parameterWithName("idParcela").description("Id da parcela para busca")),
							relaxedResponseFields(
									subsectionWithPath("idParcela").description("Identificador da parcela"),
									subsectionWithPath("valor").description("Valor do pagamento"),
									subsectionWithPath("pago").description("Situação de pagamento"),
									subsectionWithPath("dataVencimento").description("data de vencimento")
									)
							))
					.andReturn();
			
			Parcela parcelaRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Parcela.class);
			System.out.println("Parcela retorno:" + mapper.writeValueAsString(parcelaRetorno));
			assertEquals(parcelaTeste.getIdParcela(), parcelaRetorno.getIdParcela());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testAtualizar() {
		parcelaTeste.setValor(new BigDecimal(200));
		
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.put("/parcela")
					.content(mapper.writeValueAsString(parcelaTeste))
					.contentType(MediaType.APPLICATION_JSON)
					.header("Authorization", authToken))
					.andExpect(status().isAccepted())
					.andDo(document("atualizar",
							relaxedRequestFields(
									fieldWithPath("idParcela").description("Identificador da parcela"),
									fieldWithPath("valor").description("Valor do pagamento"),
									fieldWithPath("pago").description("Situação de pagamento"),
									fieldWithPath("dataVencimento").description("data de vencimento")
							)))
					.andReturn();
			Parcela parcelaRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Parcela.class);
			assertEquals(parcelaTeste.getValor(), parcelaRetorno.getValor());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
