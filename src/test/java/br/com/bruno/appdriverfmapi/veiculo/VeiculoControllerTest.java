package br.com.bruno.appdriverfmapi.veiculo;

import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.bruno.appdriverfmapi.AppdriverfmapiApplication;
import br.com.bruno.appdriverfmapi.autenticacao.AutenticacaoServicos;
import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioAutoridade;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppdriverfmapiApplication.class)
@WebAppConfiguration
public class VeiculoControllerTest {

	@Autowired
	private WebApplicationContext applicationContext;
	
	@Autowired
	private ObjectMapper mapper;
		
	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/snippets");
	
	private String authToken;
	private Veiculo primeiroVeiculo;
	private MockMvc mvc;

	
	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(applicationContext).apply(documentationConfiguration(restDocumentation)).build();
		Usuario usuarioTeste = new Usuario().setIdUsuario(1L).setAutoridade(UsuarioAutoridade.MOTORISTA).setEmail("usuario@dominio.com.br");
		authToken = AutenticacaoServicos.gerarToken(usuarioTeste);
		primeiroVeiculo = new Veiculo().setDescricao("Descrição aqui").setNome("Nome para referência").setPlaca("AAA-9999");
		
	}

	
	@Test
	public void testController() throws JsonProcessingException, Exception {
		this.testCriar();
		this.testListar();
		this.testBuscarPorId();
		this.testAtualiar();
		this.testExcluir();
	}

	private void testCriar() throws JsonProcessingException, Exception {
		String strParams = mapper.writeValueAsString(primeiroVeiculo);
		MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
			.post("/veiculo")
			.contentType(MediaType.APPLICATION_JSON)
			.content(strParams)
			.header("Authorization", authToken))
				.andExpect(status().isCreated())
			.andDo(document(
					"criar-veiculo", 
					relaxedRequestFields(
							fieldWithPath("nome").description("Nome para referência ao veículo - obrigatório"),
							fieldWithPath("descricao").description("Texto descritivo a cerca - opcional"),
							fieldWithPath("placa").description("Número do emplacamento - opcional, não pode conter mais de 9 caracteres")
							)
					))
			.andReturn();
		primeiroVeiculo.setIdVeiculo(
				Utilitario.jsonHateoasParaObjeto(
						resultado.getResponse().getContentAsString(), Veiculo.class).getIdVeiculo());		
	}

	private void testAtualiar() throws JsonProcessingException, Exception {
		
		primeiroVeiculo.setNome("Novo nome");		
		MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
			.put("/veiculo")
			.contentType(MediaType.APPLICATION_JSON)
			.content(mapper.writeValueAsString(primeiroVeiculo))
			.header("Authorization", authToken))
			.andDo(document(
					"atualizar-veiculo", 
					requestFields(
							fieldWithPath("idVeiculo").description("Identificador individual do veículo"),
							fieldWithPath("nome").description("Nome para referência ao veículo - obrigatório"),
							fieldWithPath("descricao").description("Texto descritivo a cerca - opcional"),
							fieldWithPath("placa").description("Número do emplacamento - opcional, não pode conter mais de 9 caracteres")
							)
					))
			.andExpect(status().isAccepted())
			.andReturn();
		
		Veiculo veiculoRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Veiculo.class);
		
		assertEquals(primeiroVeiculo.getNome(), veiculoRetorno.getNome());
	}

	
	private void testExcluir() throws Exception {
		mvc.perform(RestDocumentationRequestBuilders
				.delete("/veiculo/{id}", primeiroVeiculo.getIdVeiculo())
				.header("Authorization", authToken))
				.andDo(document("buscar-veiculo", 
						pathParameters(
								parameterWithName("id").description("Identificador do veículo para busca."))
						))
				.andExpect(status().isOk());
		
	}
	
	
	private void testListar() {
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.get("/veiculo")
					.header("Authorization", authToken))
					.andDo(document("listar-veiculos", 
							relaxedResponseFields(
									subsectionWithPath("_embedded").description("Vetor de veículos")
									)
							))
					.andReturn();
			System.out.println(resultado.getResponse().getContentAsString());
			
		} catch (Exception e) {
			e.printStackTrace();
		}
				
	}

	
	private void testBuscarPorId() throws Exception {
		MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
				.get("/veiculo/{idVeiculo}", primeiroVeiculo.getIdVeiculo().toString())
				.header("Authorization", authToken))
				.andDo(document("buscar-veiculo", 
						pathParameters(
								parameterWithName("idVeiculo").description("Identificador do veículo para busca."))
						
						))
				.andReturn();
		assertEquals(Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Veiculo.class).getNome(), primeiroVeiculo.getNome());
	}

}
