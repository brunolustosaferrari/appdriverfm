package br.com.bruno.appdriverfmapi.usuario;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;

import java.net.URI;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.bruno.appdriverfmapi.AppdriverfmapiApplication;
import br.com.bruno.appdriverfmapi.autenticacao.AutenticacaoServicos;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;
import junit.framework.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppdriverfmapiApplication.class)
@Transactional
@WebAppConfiguration
public class UsuarioControllerTest extends TestCase  {
	
	protected MockMvc mockMvc;
	protected ObjectMapper mapper;
	protected String tokenAutenticacao;
	
	@Autowired
	WebApplicationContext applicationContext;
	
	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/snippets");

	@Before
	public void setUp() throws Exception {
		mockMvc = MockMvcBuilders.webAppContextSetup(applicationContext).apply(documentationConfiguration(restDocumentation)).build();
		mapper = new ObjectMapper();
		mapper.registerModule(new Jackson2HalModule());
		
		
	}

	
	@Test
	public void testCrud() throws JsonProcessingException, Exception {
		Usuario usuarioTeste = new Usuario();
		usuarioTeste.setEmail("teste@teste.com").setSenha("123456").setAutoridade(UsuarioAutoridade.MOTORISTA);
		testCriar(usuarioTeste);
		tokenAutenticacao = AutenticacaoServicos.gerarToken(usuarioTeste);
		testBuscarPorId(usuarioTeste);
		usuarioTeste.setEmail("email2@email.com");
		testAtualizar(usuarioTeste);
		testExcluir(usuarioTeste);
	}
	
	protected void testBuscarPorId(Usuario usuarioTeste) throws Exception {

		MvcResult resultado = mockMvc.perform(RestDocumentationRequestBuilders
				.get("/usuario/{id}", usuarioTeste.getIdUsuario().toString())
				.header("Authorization", tokenAutenticacao)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andDo(document("usuario",
						pathParameters(parameterWithName("id").description("Id do Usuário para busca")),
						responseFields(subsectionWithPath("idUsuario")
						          .description("Identificador primario do usuário"), 
						          subsectionWithPath("email")
						          .description("Email do Usuário"),
						          subsectionWithPath("_links")
						          .description("Links suportados para o recurso"))
						))
				.andReturn();
				
		assertEquals(usuarioTeste.getEmail(), Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Usuario.class).getEmail());
		System.out.println(resultado.getResponse().getContentAsString());
		Long idNaoExiste = usuarioTeste.getIdUsuario() + 1;
		URI requestUriNaoExiste = URI.create("/usuario/".concat(idNaoExiste.toString()));
		MvcResult resultadoNaoEncontrado = mockMvc.perform(RestDocumentationRequestBuilders
				.get(requestUriNaoExiste)
				.header("Authorization", tokenAutenticacao)
				.contentType(MediaType.APPLICATION_JSON))
				.andReturn();
		assertEquals(401, resultadoNaoEncontrado.getResponse().getStatus());

	}

	protected void testCriar(Usuario usuarioTeste) throws JsonProcessingException, Exception {
		
		java.util.Map<String, Object> objetoMapeado = Utilitario.mappearObjeto(usuarioTeste);
				
		MvcResult resultado = mockMvc.perform(MockMvcRequestBuilders
				.post("/usuario")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(objetoMapeado)))
				.andExpect(status().isCreated())
				.andExpect(jsonPath("$.email", is(usuarioTeste.getEmail())))
				.andReturn();
		
		String strJson = resultado.getResponse().getContentAsString();
		Usuario resultadoJson = Utilitario.jsonHateoasParaObjeto(strJson, Usuario.class);
		usuarioTeste.setIdUsuario(resultadoJson.getIdUsuario());
		
		mockMvc.perform(MockMvcRequestBuilders
				.post("/usuario")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(objetoMapeado)))
				.andExpect(status().isConflict())
				.andReturn();
	}

	protected void testAtualizar(Usuario usuarioTeste) throws JsonProcessingException, Exception {
		java.util.Map<String, Object> objetoMapeado = Utilitario.mappearObjeto(usuarioTeste);
		MvcResult resultado = mockMvc.perform(RestDocumentationRequestBuilders
				.put("/usuario/{id}", usuarioTeste.getIdUsuario().toString())
				.header("Authorization", tokenAutenticacao)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(objetoMapeado)))
				.andExpect(status().isAccepted())
				.andExpect(jsonPath("$.email", is(usuarioTeste.getEmail())))
				.andDo(document("atualizar-usuario", 
						requestFields(
								fieldWithPath("idUsuario").description("Id do Usuario"),
								fieldWithPath("email").description("Email do Usuario"),
								fieldWithPath("senha").description("Senha do Usuario"),
								fieldWithPath("autoridade").description("Autoridade do Usuario - não é alterável e pode ser omitido")),
						pathParameters(parameterWithName("id").description("Id do usuario a ser atualizado - necessário para fins de autenticação."))
						))
				.andReturn();
		
		System.out.println(resultado.getResponse().getContentAsString());
		
		Usuario usuarioNaoExiste = new Usuario();
		usuarioNaoExiste.setIdUsuario(usuarioTeste.getIdUsuario() + 1);
		usuarioNaoExiste.setEmail("teste2@email.com");
		usuarioNaoExiste.setSenha("123456");
		usuarioNaoExiste.setAutoridade(UsuarioAutoridade.MOTORISTA);
		
		java.util.Map<String, Object> objetoMapeadoNaoExiste = Utilitario.mappearObjeto(usuarioNaoExiste);
				
		MvcResult resultadoNaoExiste = mockMvc.perform(MockMvcRequestBuilders
				.put( ("/usuario/").concat(usuarioNaoExiste.getIdUsuario().toString()) )
				.header("Authorization", tokenAutenticacao)
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(objetoMapeadoNaoExiste)))
				.andReturn();
		
		assertEquals(401, resultadoNaoExiste.getResponse().getStatus());
	}

	protected void testExcluir(Usuario usuarioTeste) throws Exception {
		MvcResult resultado = mockMvc.perform(RestDocumentationRequestBuilders
				.delete("/usuario/{id}", usuarioTeste.getIdUsuario().toString())
				.header("Authorization", tokenAutenticacao)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(document("excluir-usuario", pathParameters(parameterWithName("id").description("Id do usuario a ser excluido"))))
				.andReturn();
		
		assertEquals(204, resultado.getResponse().getStatus());
		
		MvcResult resultadoPosExclusao = mockMvc.perform(MockMvcRequestBuilders
				.delete("/usuario/".concat(usuarioTeste.getIdUsuario().toString()))
				.header("Authorization", tokenAutenticacao)
				.contentType(MediaType.APPLICATION_JSON))
				.andReturn();
		
		assertEquals(401, resultadoPosExclusao.getResponse().getStatus());

	}

}
