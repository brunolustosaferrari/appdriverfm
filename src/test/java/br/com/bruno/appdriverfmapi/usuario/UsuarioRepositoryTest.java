package br.com.bruno.appdriverfmapi.usuario;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import junit.framework.TestCase;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class UsuarioRepositoryTest extends TestCase  {
	
	@Autowired
	UsuarioRepository repositorio;
	Usuario usuarioTeste; 

	@Before
	public void setUp() throws Exception {
		usuarioTeste = new Usuario();
		usuarioTeste.setEmail("teste@teste.com");
		usuarioTeste.setSenha("123456");
		usuarioTeste.setAutoridade(UsuarioAutoridade.MOTORISTA);
	}

	@After
	public void tearDown() throws Exception {
		repositorio.delete(usuarioTeste);
	}

	@Test
	public void testSaveFindByEmailAndSenha() {
		repositorio.save(usuarioTeste);
		assertNotNull(repositorio.findByEmailAndSenha(usuarioTeste.getEmail(), usuarioTeste.getSenha()));
		assertNotNull(repositorio.findByEmail(usuarioTeste.getEmail()));
		assertEquals(UsuarioAutoridade.MOTORISTA, repositorio.findByEmail(usuarioTeste.getEmail()).getAutoridade());
	}

}
