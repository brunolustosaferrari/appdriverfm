package br.com.bruno.appdriverfmapi.usuario;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import junit.framework.Test;
import junit.framework.TestSuite;

@RunWith(Suite.class)
@SuiteClasses({ UsuarioControllerTest.class, UsuarioRepositoryTest.class })
public class AllTests {
	
	public static Test suite() {
		TestSuite suite = new TestSuite("Testes para Usuario");
		suite.addTestSuite(UsuarioRepositoryTest.class);
		suite.addTestSuite(UsuarioControllerTest.class);
		return suite;
		
	}
}
