package br.com.bruno.appdriverfmapi.diaria;

import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.bruno.appdriverfmapi.AppdriverfmapiApplication;
import br.com.bruno.appdriverfmapi.aplicativo.Aplicativo;
import br.com.bruno.appdriverfmapi.aplicativo.AplicativoRepository;
import br.com.bruno.appdriverfmapi.autenticacao.AutenticacaoServicos;
import br.com.bruno.appdriverfmapi.multiTenancy.ContextoInquilino;
import br.com.bruno.appdriverfmapi.rendimentoDiario.RendimentoDiario;
import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioRepository;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;
import br.com.bruno.appdriverfmapi.veiculo.Veiculo;
import br.com.bruno.appdriverfmapi.veiculo.VeiculoRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppdriverfmapiApplication.class)
@WebAppConfiguration
public class DiariaControllerTest {
	
	@Autowired
	private WebApplicationContext applicationContext;
	
	@Autowired
	private ObjectMapper mapper;
		
	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/snippets");
	
	private Veiculo veiculoTeste;
	private Aplicativo aplicativoTeste;
	private Veiculo veiculoTeste1;
	private Aplicativo aplicativoTeste1;
	private Diaria diariaTeste;
	private String authToken;
	private MockMvc mvc;
	private Usuario usuarioTeste;
	private RendimentoDiario rendimentoDiarioTeste1;
	private RendimentoDiario rendimentoDiarioTeste2;

	
	@Autowired 
	private VeiculoRepository repositorioVeiculo;
	@Autowired 
	private AplicativoRepository repositorioAplicativo;
	@Autowired
	UsuarioRepository repositorioUsuario;
	
	@Before
	public void setUp() throws Exception {
		mvc = MockMvcBuilders.webAppContextSetup(applicationContext).apply(documentationConfiguration(restDocumentation)).build();
		usuarioTeste = repositorioUsuario.findByEmail("testebase@teste.com");
				
		authToken = AutenticacaoServicos.gerarToken(usuarioTeste);
		
		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		
		aplicativoTeste = repositorioAplicativo.findByNome("Aplicativo");
		if(aplicativoTeste == null) {
			aplicativoTeste = new Aplicativo()
								.setNome("Aplicativo")
								.setDescricao("Descrição aqui");
			repositorioAplicativo.save(aplicativoTeste);
		}

		veiculoTeste = new Veiculo().setDescricao("Descrição aqui").setNome("Nome para referência").setPlaca("AAA-9999");
		repositorioVeiculo.save(veiculoTeste);
		
		aplicativoTeste1 = repositorioAplicativo.findByNome("Aplicativo 2");
		if(aplicativoTeste1 == null) {
			aplicativoTeste1 = new Aplicativo()
								.setNome("Aplicativo 2")
								.setDescricao("Descrição aqui");
			repositorioAplicativo.save(aplicativoTeste1);
		}

		veiculoTeste1 = new Veiculo().setDescricao("Descrição aqui").setNome("Nome para referência").setPlaca("BBB-1111");
		repositorioVeiculo.save(veiculoTeste1);
		
		ContextoInquilino.limpar();
		
		Calendar dataHoraInicio = Calendar.getInstance();
		Calendar dataHoraFim = Calendar.getInstance();
		SimpleDateFormat formatoDataHora = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
		SimpleDateFormat formatoHora = new SimpleDateFormat("hh:mm:ss");
		dataHoraInicio.setTime(formatoDataHora.parse("12/12/2012 00:00:00"));
		dataHoraFim.setTime(formatoDataHora.parse("12/12/2012 06:00:00"));
		
		diariaTeste = new Diaria()
				.setDataHoraInicio(dataHoraInicio)
				.setDataHoraFim(dataHoraFim);
		
		rendimentoDiarioTeste1 = new RendimentoDiario()
				.setAplicativo(aplicativoTeste)
				.setVeiculo(veiculoTeste)
				.setDistanciaPercorridaEmKm((float) 1000)
				.setTempoEmCorrida(new Time(formatoHora.parse("04:30:00").getTime()))
				.setValorRecebido(new BigDecimal(150.00));
		
		rendimentoDiarioTeste2 = new RendimentoDiario()
				.setAplicativo(aplicativoTeste1)
				.setVeiculo(veiculoTeste1)
				.setDistanciaPercorridaEmKm((float) 1000)
				.setTempoEmCorrida(new Time(formatoHora.parse("01:00:00").getTime()))
				.setValorRecebido(new BigDecimal(50.00));
		List<RendimentoDiario> listaRendimentos = new ArrayList<RendimentoDiario>();
		listaRendimentos.add(rendimentoDiarioTeste1);
		listaRendimentos.add(rendimentoDiarioTeste2);
		diariaTeste.setRendimentosDiarios(listaRendimentos);
	}	

	@After
	public void tearDown() throws Exception {
		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		repositorioAplicativo.delete(aplicativoTeste);
		repositorioVeiculo.delete(veiculoTeste);
		repositorioAplicativo.delete(aplicativoTeste1);
		repositorioVeiculo.delete(veiculoTeste1);
	}
	
	@Test
	public void testController() {
		this.testCriar();
		this.testListar();
		this.testBuscarPorId();
		this.testAtualizar();
		this.testExcluir();
	}
	
	private void testListar() {
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
									.get("/diaria")
									.header("Authorization", authToken))
									.andExpect(status().isOk())
									.andDo(document("listar",
											relaxedResponseFields(
													subsectionWithPath("_embedded").description("Vetor de diarias")
													)
											))
									.andReturn();
			System.out.println(resultado.getResponse().getContentAsString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void testBuscarPorId() {
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.get("/diaria/{id}", diariaTeste.getIdDiaria())
					.header("Authorization", authToken))
					.andExpect(status().isOk())
					.andDo(document("buscar-por-id",
							pathParameters(
									parameterWithName("id").description("Id da diária para busca")),
							relaxedResponseFields(
									subsectionWithPath("idDiaria").description("Identificador da Diaria"),
									subsectionWithPath("dataHoraFim").description("Data e hora de encerramento"),
									subsectionWithPath("dataHoraInicio").description("Data e hora de início"),
									subsectionWithPath("rendimentosDiarios").description("Vetor de rendimentos diários"),
									subsectionWithPath("rendimentosDiarios[].aplicativo").description("Aplicativo"),
									subsectionWithPath("rendimentosDiarios[].veiculo").description("Veiculo"),
									subsectionWithPath("rendimentosDiarios[].distanciaPercorridaEmKm").description("Distância percorrida ao longo da diária"),
									subsectionWithPath("rendimentosDiarios[].tempoEmCorrida").description("Tempo gasto em corridas para o aplicativo ao longo da diária"),
									subsectionWithPath("rendimentosDiarios[].valorRecebido").description("Valor recebido do aplicativo")
									)
							))
					.andReturn();
			Diaria diariaRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Diaria.class);
			System.out.println(mapper.writeValueAsString(diariaRetorno));
			assertEquals(diariaTeste.getIdDiaria(), diariaRetorno.getIdDiaria());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void testCriar() {
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.post("/diaria")
					.contentType(MediaType.APPLICATION_JSON)
					.content(mapper.writeValueAsString(diariaTeste))
					.header("Authorization", authToken))
					.andExpect(status().isCreated())
					.andDo(document("criar",
							relaxedRequestFields(
									fieldWithPath("dataHoraFim").description("Data e hora de encerramento. No formato ISO 8601"),
									fieldWithPath("dataHoraInicio").description("Data e hora de início. No formato ISO 8601"),
									fieldWithPath("rendimentosDiarios").description("Vetor de rendimentos diários"),
									fieldWithPath("rendimentosDiarios[].aplicativo").description("Aplicativo"),
									fieldWithPath("rendimentosDiarios[].veiculo").description("Veiculo"),
									fieldWithPath("rendimentosDiarios[].distanciaPercorridaEmKm").description("Distância percorrida ao longo da diária"),
									fieldWithPath("rendimentosDiarios[].tempoEmCorrida").description("Tempo gasto em corridas para o aplicativo ao longo da diária. No formato ISO hh:mm:ss"),
									fieldWithPath("rendimentosDiarios[].valorRecebido").description("Valor recebido do aplicativo")
									)
							))
					.andReturn();
			System.out.println(resultado.getResponse().getContentAsString());
			Diaria diariaRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Diaria.class);
			diariaTeste = diariaRetorno;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void testAtualizar() {
		diariaTeste.getRendimentosDiarios().get(0).setDistanciaPercorridaEmKm((float) 9999.99);
		diariaTeste.getRendimentosDiarios().remove(1);
		
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.put("/diaria")
					.content(mapper.writeValueAsString(diariaTeste))
					.contentType(MediaType.APPLICATION_JSON)
					.header("Authorization", authToken))
					.andExpect(status().isAccepted())
					.andDo(document("atualizar",
							relaxedRequestFields(
									fieldWithPath("dataHoraFim").description("Data e hora de encerramento. No formato  ISO 8601"),
									fieldWithPath("dataHoraInicio").description("Data e hora de início. No formato  ISO 8601"),
									fieldWithPath("rendimentosDiarios[]").description("Vetor de Rendimentos"),
									fieldWithPath("rendimentosDiarios[].aplicativo").description("Aplicativo"),
									fieldWithPath("rendimentosDiarios[].veiculo").description("Veiculo"),
									fieldWithPath("rendimentosDiarios[].distanciaPercorridaEmKm").description("Distância percorrida ao longo da diária"),
									fieldWithPath("rendimentosDiarios[].tempoEmCorrida").description("Tempo gasto em corridas para o aplicativo ao longo da diária. No formato ISO hh:mm:ss"),
									fieldWithPath("rendimentosDiarios[].valorRecebido").description("Valor recebido do aplicativo")
									)
							))
					.andReturn();
			System.out.println(resultado.getResponse().getContentAsString());
			Diaria diariaRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), Diaria.class);
			assertEquals(diariaTeste.getRendimentosDiarios().get(0).getDistanciaPercorridaEmKm(), diariaRetorno.getRendimentosDiarios().get(0).getDistanciaPercorridaEmKm(), 0 );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void testExcluir() {
		try {
			mvc.perform(RestDocumentationRequestBuilders
					.delete("/diaria/{id}", diariaTeste.getIdDiaria())
					.header("Authorization", authToken))
					.andExpect(status().isNoContent())
					.andDo(document("buscar-por-id",
							pathParameters(
									parameterWithName("id").description("Id da diária para busca"))
									)
							);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
