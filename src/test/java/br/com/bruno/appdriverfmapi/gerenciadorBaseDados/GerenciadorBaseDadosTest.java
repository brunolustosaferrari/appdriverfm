package br.com.bruno.appdriverfmapi.gerenciadorBaseDados;

import static org.junit.Assert.assertFalse;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioAutoridade;
import br.com.bruno.appdriverfmapi.usuario.UsuarioRepository;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;

@RunWith(SpringJUnit4ClassRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class GerenciadorBaseDadosTest {

	GerenciadorBaseDados gerenciadorBase;
	Usuario usuario;
	Connection conexao; 
	
	@Autowired
	UsuarioRepository repositorioUsuario;
	
	
	@Before
	public void setUp() throws Exception {
		
		usuario = new Usuario()
				.setEmail("testebase@teste.com")
				.setSenha(Utilitario.gerarHash256("123456"))
				.setAutoridade(UsuarioAutoridade.MOTORISTA);
		
		Connection conexaoUsuario = ConnectionFactory.getConnection();
		
		PreparedStatement stmt = conexaoUsuario.prepareStatement("INSERT INTO usuario (email, senha, autoridade) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS);
		stmt.setString(1, usuario.getEmail());
		stmt.setString(2, usuario.getSenha());
		stmt.setString(3, usuario.getAutoridade().toString());
		stmt.execute();
		ResultSet resultado = stmt.getGeneratedKeys();
		resultado.next();
		usuario.setIdUsuario(resultado.getLong(1));
		System.out.println("Id:" + usuario.getIdUsuario().toString());
		gerenciadorBase = new GerenciadorBaseDados(usuario);
		
		conexaoUsuario.close();
	}

	@After
	public void tearDown() throws Exception {
		if(!conexao.isClosed()) {
			conexao.close();
		}
	}

	@Test
	public void testCriarBase() throws NoSuchAlgorithmException, SQLException {
		try {
		gerenciadorBase.criarBase();
		conexao = ConnectionFactory.getConnection(usuario.getNomeSchema());
		assertFalse(conexao.isClosed());	
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}
