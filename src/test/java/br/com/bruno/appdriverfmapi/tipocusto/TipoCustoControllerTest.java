package br.com.bruno.appdriverfmapi.tipocusto;

import static org.junit.Assert.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.relaxedRequestFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.relaxedResponseFields;
import static org.springframework.restdocs.payload.PayloadDocumentation.subsectionWithPath;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.bruno.appdriverfmapi.AppdriverfmapiApplication;
import br.com.bruno.appdriverfmapi.autenticacao.AutenticacaoServicos;
import br.com.bruno.appdriverfmapi.multiTenancy.ContextoInquilino;
import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioRepository;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = AppdriverfmapiApplication.class)
@WebAppConfiguration
public class TipoCustoControllerTest {
	
	@Autowired
	private WebApplicationContext applicationContext;
	
	@Autowired
	private ObjectMapper mapper;
	
	@Autowired
	UsuarioRepository repositorioUsuario;
	
	@Autowired
	private TipoCustoRepository repositorio;
	
	@Rule
	public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/snippets");
	
	private String authToken;
	private MockMvc mvc;
	private Usuario usuarioTeste;
	private TipoCusto tipoCustoTeste;
	
	@Before
	public void setUp() throws Exception {
		
		mvc = MockMvcBuilders.webAppContextSetup(applicationContext).apply(documentationConfiguration(restDocumentation)).build();
		usuarioTeste = repositorioUsuario.findByEmail("testebase@teste.com");
		authToken = AutenticacaoServicos.gerarToken(usuarioTeste);
		tipoCustoTeste = new TipoCusto().setDescricao("Tipo de custo teste");
	}

//	@Test
//	public void test() {
//		this.testCriar();
//		this.testBuscarPorId();
//		this.testAtualizar();
//		this.testExcluir();
//	}
	
	@Test
	public void testCriar() {
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.post("/tipo-custo")
					.contentType(MediaType.APPLICATION_JSON)
					.content(mapper.writeValueAsString(tipoCustoTeste))
					.header("Authorization", authToken))
					.andExpect(status().isCreated())
					.andDo(document("criar",
							relaxedRequestFields(
									fieldWithPath("descricao").description("Descrição do tipo de custo. Máximo de 45 caracteres. ")
									)
							))
					.andReturn();
			
			System.out.println(resultado.getResponse().getContentAsString());
			TipoCusto tipoRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), TipoCusto.class);
			assertEquals(tipoCustoTeste.getDescricao(), tipoRetorno.getDescricao());
			ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
			repositorio.delete(tipoRetorno);
			ContextoInquilino.limpar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAtualizar() {

		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		tipoCustoTeste = repositorio.save(tipoCustoTeste);
		tipoCustoTeste.setDescricao("Nova descrição atualizada");
		ContextoInquilino.limpar();
		
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.put("/tipo-custo")
					.content(mapper.writeValueAsString(tipoCustoTeste))
					.contentType(MediaType.APPLICATION_JSON)
					.header("Authorization", authToken))
					.andExpect(status().isAccepted())
					.andDo(document("atualizar",
							relaxedRequestFields(
									fieldWithPath("descricao").description("Descrição do tipo de custo. Máximo de 45 caracteres. "))
							))
					.andReturn();
			System.out.println(resultado.getResponse().getContentAsString());
			TipoCusto tipoRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), TipoCusto.class);
			assertEquals(tipoCustoTeste.getDescricao(), tipoRetorno.getDescricao());
			ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
			repositorio.delete(tipoRetorno);
			ContextoInquilino.limpar();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testExcluir() {
		
		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		tipoCustoTeste = repositorio.save(tipoCustoTeste);
		ContextoInquilino.limpar();
		
		try {
			mvc.perform(RestDocumentationRequestBuilders
					.delete("/tipo-custo/{idTipoCusto}", tipoCustoTeste.getIdTipoCusto())
					.header("Authorization", authToken))
					.andExpect(status().isNoContent())
					.andDo(document("excluir",
							pathParameters(
									parameterWithName("idTipoCusto").description("Id do tipo para exclusão"))
									)
							);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void testBuscarPorId() {
		
		ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
		tipoCustoTeste = repositorio.save(tipoCustoTeste);
		ContextoInquilino.limpar();
		try {
			MvcResult resultado = mvc.perform(RestDocumentationRequestBuilders
					.get("/tipo-custo/{idTipoCusto}", tipoCustoTeste.getIdTipoCusto())
					.header("Authorization", authToken))
					.andExpect(status().isOk())
					.andDo(document("buscar-por-id",
							pathParameters(
									parameterWithName("idTipoCusto").description("Id do tipo para busca")),
							relaxedResponseFields(
									subsectionWithPath("idTipoCusto").description("Identificador do tipo de custo"),
									subsectionWithPath("descricao").description("Descrição do tipo de custo")
									)
							))
					.andReturn();
			
			TipoCusto tipoRetorno = Utilitario.jsonHateoasParaObjeto(resultado.getResponse().getContentAsString(), TipoCusto.class);
			assertEquals(tipoCustoTeste.getDescricao(), tipoRetorno.getDescricao());
			ContextoInquilino.setInquilinoAtual(usuarioTeste.getNomeSchema());
			repositorio.delete(tipoRetorno);
			ContextoInquilino.limpar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
