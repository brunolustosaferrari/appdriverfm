package br.com.bruno.appdriverfmapi.faturamento;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class FaturamentoController {
	
	@Autowired
	private LocalContainerEntityManagerFactoryBean emf;
	
	@Autowired
	private ObjectMapper mapper;
	
	@GetMapping("/faturamento/{ano}")
	public ResponseEntity<?> buscarFaturamentoMensal(@PathVariable String ano){
		try {
			EntityManager entityManager = emf.getObject().createEntityManager();
			FaturamentoDao dao = new FaturamentoDao(entityManager);
			Object faturamentoPorPeriodo = dao.getFaturamentoMensal(ano);
			return ResponseEntity.ok(mapper.writeValueAsString(faturamentoPorPeriodo));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Ocorreu um erro ao gerar o relatório");
		}
	}
	
}
