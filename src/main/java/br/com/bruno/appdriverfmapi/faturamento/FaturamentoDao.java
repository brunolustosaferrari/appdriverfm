package br.com.bruno.appdriverfmapi.faturamento;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

/**
 * Objeto de acesso a dados responsável por buscar a lista de faturamentos na base.
 * 
 */
public class FaturamentoDao {
	
	private EntityManager entityManager;
	
	FaturamentoDao(EntityManager entityManager){
		this.entityManager = entityManager;
	}
	
	@SuppressWarnings("unchecked")
	public Map<String, List<Faturamento>>  getFaturamentoMensal(String ano){
		String queryString = "SELECT \n" + 
							"    SUM(valorRecebido) AS valor,\n" + 
							"    SEC_TO_TIME(SUM(TIME_TO_SEC(tempoEmCorrida))) AS tempoEmCorrida,\n" + 
							"    SUM(distanciaPercorridaEmKm) AS distanciaPercorrida,\n" + 
							"    DATE_FORMAT(diaria.dataHoraFim, '%m/%Y') AS periodo,\n" +
							"	 aplicativo.idAplicativo as idAplicativo,\n	" +
							"    aplicativo.nome as nomeAplicativo,\n " +
							"    aplicativo.descricao as descricaoAplicativo\n" + 
							"FROM\n" + 
							"    rendimentoDiario\n" + 
							"        JOIN\n" + 
							"    aplicativo ON rendimentoDiario.idAplicativo = aplicativo.idAplicativo\n" + 
							"        JOIN\n" + 
							"    diaria ON rendimentoDiario.idDiaria = diaria.idDiaria\n" + 
							"WHERE\n" + 
							"    YEAR(diaria.dataHoraFim) = " + ano + "\n" + 
							"GROUP BY MONTH(diaria.dataHoraFim) , rendimentoDiario.idAplicativo";	
		
		
		List<Faturamento> faturamentoMensal = entityManager.createNativeQuery(queryString, "FaturamentoResult").getResultList();
		Map<String, List<Faturamento>> mapaPorPeriodo = new HashMap<String, List<Faturamento>>();
		faturamentoMensal.forEach((faturamento) -> {
			if(!mapaPorPeriodo.containsKey(faturamento.getPeriodo())) {
				mapaPorPeriodo.put(faturamento.getPeriodo(), new ArrayList<Faturamento>());
			}
			mapaPorPeriodo.get(faturamento.getPeriodo()).add(faturamento);
		});
		return mapaPorPeriodo;
	}

}
