package br.com.bruno.appdriverfmapi.faturamento;

import java.math.BigDecimal;

import br.com.bruno.appdriverfmapi.aplicativo.Aplicativo;
import br.com.bruno.appdriverfmapi.rendimentoDiario.RendimentoDiario;
/**
 * Modelo do faturamento de um aplicativo. Deve ser usado junto a {@link FaturamentosPorPeriodo} para agrupar os rendimentos de um determinado periodo de tempo.
 * SQlResultSet definido em {@link RendimentoDiario}
 *
 */
public class Faturamento {
	
	private Aplicativo aplicativo;
	private float distanciaPercorrida;
	private String tempoEmCorrida;
	private BigDecimal valor;
	private String periodo;
	
	public Faturamento(Long idAplicativo, String nomeAplicativo, String descricaoAplicativo, float distanciaPercorrida, String tempoEmCorrida, BigDecimal valor, String periodo){
		this.aplicativo = new Aplicativo().setIdAplicativo(idAplicativo).setNome(nomeAplicativo).setDescricao(descricaoAplicativo);
		this.distanciaPercorrida = distanciaPercorrida;
		this.tempoEmCorrida = tempoEmCorrida;
		this.valor = valor;
		this.periodo = periodo;
	}
	
	public Aplicativo getAplicativo() {
		return aplicativo;
	}
	public void setAplicativo(Aplicativo aplicativo) {
		this.aplicativo = aplicativo;
	}
	public float getDistanciaPercorrida() {
		return distanciaPercorrida;
	}
	public void setDistanciaPercorrida(float distanciaPercorrida) {
		this.distanciaPercorrida = distanciaPercorrida;
	}
	public String getTempoEmCorrida() {
		return tempoEmCorrida;
	}
	public void setTempoEmCorrida(String tempoEmCorrida) {
		this.tempoEmCorrida = tempoEmCorrida;
	}
	public BigDecimal getValor() {
		return valor;
	}
	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
	public String getPeriodo() {
		return periodo;
	}
	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

}
