package br.com.bruno.appdriverfmapi.tipocusto;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;


/**
 * Class responsável por modelar um tipo de custo
 *
 */
@Entity
@Table(name="tipoCusto")
public class TipoCusto {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idTipoCusto;
	
	@Size(max=45)
	private String descricao;
	
	public Long getIdTipoCusto() {
		return idTipoCusto;
	}
	public TipoCusto setIdTipoCusto(Long idTipoCusto) {
		this.idTipoCusto = idTipoCusto;
		return this;
	}
	public String getDescricao() {
		return descricao;
	}
	public TipoCusto setDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}
}
