package br.com.bruno.appdriverfmapi.tipocusto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoCustoRepository extends JpaRepository<TipoCusto, Long> {

}
