package br.com.bruno.appdriverfmapi.tipocusto;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class TipoCustoResourceAssembler implements ResourceAssembler<TipoCusto, Resource<TipoCusto>> {

	@Override
	public Resource<TipoCusto> toResource(TipoCusto entity) {
		return new Resource<>( entity, linkTo( methodOn(TipoCustoController.class).buscar(entity.getIdTipoCusto()) ).withSelfRel() );
	}

}
