package br.com.bruno.appdriverfmapi.tipocusto;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class TipoCustoController {

	@Autowired
	private TipoCustoRepository repositorio;
	
	@Autowired
	private TipoCustoResourceAssembler assembler;
	
	@GetMapping("tipo-custo")
	public Resources<Resource<TipoCusto>> listar() {
		List<Resource<TipoCusto>> listaTipos =  repositorio.findAll().stream()				
															.map( tipoCusto -> assembler.toResource(tipoCusto) )
															.collect(Collectors.toList());
		return new Resources<>(listaTipos, linkTo( methodOn(TipoCustoController.class).listar() ).withSelfRel() );
	};
	
	@GetMapping("tipo-custo/{idTipoCusto}")
	public Resource<TipoCusto> buscar(@PathVariable Long idTipoCusto) {
		return repositorio.findById(idTipoCusto)
				.map( tipoCusto -> assembler.toResource(tipoCusto) )
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
	};
	
	@PostMapping("tipo-custo")
	public ResponseEntity<?> criar(@RequestBody @Valid TipoCusto tipo){
		Resource<TipoCusto> corpoResposta = assembler.toResource(repositorio.save(tipo));
		try {
			return ResponseEntity.created( new URI(corpoResposta.getId().expand().getHref()) ).body(corpoResposta);
		} catch (URISyntaxException e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Erro ao gerar resposta.");
		}
	}
	
	@PutMapping("tipo-custo")
	public ResponseEntity<?> atualizar(@RequestBody @Valid TipoCusto tipo){
		Resource<TipoCusto> corpoResposta = assembler.toResource(repositorio.save(tipo));
		return ResponseEntity.accepted().body(corpoResposta);
	}
	
	@DeleteMapping("tipo-custo/{idTipoCusto}")
	public ResponseEntity<?> excluir(@PathVariable Long idTipoCusto){
		repositorio.deleteById(idTipoCusto);
		return ResponseEntity.noContent().build();
		
	}
	
}
