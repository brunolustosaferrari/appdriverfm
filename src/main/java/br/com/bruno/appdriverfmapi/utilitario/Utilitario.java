package br.com.bruno.appdriverfmapi.utilitario;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Conjunto de serviços reaproveitados ao longo da aplicação.
 * 
 */
public abstract class Utilitario {
	
	/**
	 * Encoda uma string para um hash SHA256
	 * @param entrada String a ser processada
	 * @return Hash SHA256 da string de entrada
	 * @throws NoSuchAlgorithmException Erro do MessageDigest ao criptografar
	 */
	public static String gerarHash256(String entrada) throws NoSuchAlgorithmException {
		MessageDigest md = MessageDigest.getInstance("SHA-256");
		byte[] hashSenhaEmByte = md.digest(entrada.getBytes());
		BigInteger hashEmSimbolo = new BigInteger(1, hashSenhaEmByte);
		String saida = hashEmSimbolo.toString(16);
		return saida;
	}
	
	/**
	 * Método que processa a representação de um objeto em JSON para uma instância de sua classe
	 * @param entradaJson String com a representação em JSON/Hal de um objeto, gerada pelo Spring HATEOAS
	 * @param classeAlvo Classe do objeto a ser recuperado
	 * @return Um objeto do tipo da classe alvo, com o estado obtido da representação
	 * @throws JsonParseException Erro ao processar o conteúdo da string de entrada
	 * @throws JsonMappingException Erro ao mapear a representação de volta para o objeto
	 * @throws IOException Erro de leitura da string JSON
	 */
	public static <T> T jsonHateoasParaObjeto(String entradaJson, Class<T> classeAlvo) throws JsonParseException, JsonMappingException, IOException{
		ObjectMapper mapper = new ObjectMapper();
		return mapper.readValue(entradaJson.substring(0, entradaJson.lastIndexOf(",\"_links\"", entradaJson.length())).concat("}"), classeAlvo);
	}
	
	/**
	 * Método que mapeia um objeto, usando seus getters.
	 * @param objeto Objeto para ser mapeado
	 * @return Um Map no qual cada string equivale a um nome de atributo e seu objeto par ao valor deste.
	 * @throws IllegalArgumentException Lançada quando um getter possui parametros.
	 * @throws IllegalAccessException Lançada quando um getter não é visivel.
	 */
	public static Map<String, Object> mappearObjeto(Object objeto) throws IllegalArgumentException, IllegalAccessException{
		
		Field[] atributosObjeto = objeto.getClass().getDeclaredFields();
		Class<?> classeObjeto = objeto.getClass();
		Map<String, Object> retorno = new HashMap<String, Object>(atributosObjeto.length);
		for( Field atributoObjeto : atributosObjeto ) {
			String nomeAtributo = atributoObjeto.getName();
			String nomeGet = "get".concat((Character.toString(nomeAtributo.charAt(0)).toUpperCase()).concat(nomeAtributo.substring(1)));
			try {
				Method metodoGetAtributo = classeObjeto.getDeclaredMethod(nomeGet, (Class<?>[]) null );
				retorno.put(atributoObjeto.getName(), metodoGetAtributo.invoke(objeto, (Object[]) null));
			}catch(NoSuchMethodException | InvocationTargetException ex) {
				continue;
			}
		}
		return retorno;
	}
}
