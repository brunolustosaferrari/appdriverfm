package br.com.bruno.appdriverfmapi.veiculo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

@RestController
public class VeiculoController {
	
	@Autowired
	VeiculoRepository repositorio;
	
	@Autowired
	VeiculoResourceAssembler assembler;
	
	@GetMapping("/veiculo")
	public Resources<Resource<Veiculo>> listar() {
		List<Resource<Veiculo>> veiculos = repositorio.findByAtivo(true).stream().map(veiculo-> assembler.toResource(veiculo)).collect(Collectors.toList());
		return new Resources<>(veiculos, linkTo(methodOn(VeiculoController.class).listar()).withSelfRel());
	}
	
	@GetMapping("/veiculo/{idVeiculo}")
	public Resource<Veiculo> buscarPorId(@PathVariable Long idVeiculo){
		return repositorio.findById(idVeiculo)
				.map(veiculo -> assembler.toResource(veiculo))
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Veículo não encontrado"));
	}
	
	@PostMapping("/veiculo")
	public ResponseEntity<?> criar(@RequestBody @Valid Veiculo veiculo){

		try {
			Resource<Veiculo> corpoResposta = assembler.toResource(repositorio.save(veiculo));
			return ResponseEntity.created(new URI(corpoResposta.getId().expand().getHref())).body(corpoResposta);
		}catch(URISyntaxException ex){
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Ocorreu um erro interno ao gravar o veículo, contate o administrador do serviço");
		}
		
	}
	
	@PutMapping("/veiculo")
	public ResponseEntity<?> atualizar(@RequestBody @Valid Veiculo veiculo){
		Resource<Veiculo> corpoResposta = assembler.toResource(repositorio.save(veiculo));
		return ResponseEntity.accepted().body(corpoResposta);
	}
	
	@DeleteMapping("/veiculo/{idVeiculo}")
	public ResponseEntity<?> excluir(@PathVariable Long idVeiculo){
		
		try {
			repositorio.findById(idVeiculo)
				.map(veiculo -> repositorio.save( veiculo.setAtivo(false) ));
			return ResponseEntity.ok().build();
		}catch(IllegalArgumentException ex) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Veiculo não encontrado");
		}
	}
	
}
