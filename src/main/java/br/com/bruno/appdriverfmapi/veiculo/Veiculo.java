package br.com.bruno.appdriverfmapi.veiculo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Classe responsável por modelar um veículo.
 * 
 */
@Entity
@Table(name="veiculo")
public class Veiculo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idVeiculo;
	
	@NotBlank
	@Size(max=45)
	private String nome;
	
	@Size(max=8)
	private String placa;
	
	private String descricao;
	@JsonIgnore
	private boolean ativo = true;
	
	
	
	public Long getIdVeiculo() {
		return idVeiculo;
	}
	public Veiculo setIdVeiculo(Long idVeiculo) {
		this.idVeiculo = idVeiculo;
		return this;
	}
	public String getNome() {
		return nome;
	}
	/**
	 * 
	 * @param nome Não pode estar vazio
	 * @return Veiculo alterado.
	 */
	public Veiculo setNome(String nome) {
		this.nome = nome;
		return this;
	}
	public String getPlaca() {
		return placa;
	}
	/**
	 * 
	 * @param placa Deve possuir no máximo 9 caracteres
	 * @return Veículo Alterado
	 */
	public Veiculo setPlaca(String placa) {
		this.placa = placa;
		return this;
	}
	public String getDescricao() {
		return descricao;
	}
	public Veiculo setDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}
	
	public boolean getAtivo() {
		return this.ativo;
	}
	
	public Veiculo setAtivo(boolean ativo) {
		this.ativo = ativo;
		return this;
	}
}
