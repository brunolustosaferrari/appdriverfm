package br.com.bruno.appdriverfmapi.veiculo;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;


/**
 * Classe responsável por gerar o estado representacional de um veiculo
 *
 */
@Component
public class VeiculoResourceAssembler implements ResourceAssembler<Veiculo, Resource<Veiculo>> {

	
	@Override
	public Resource<Veiculo> toResource(Veiculo veiculo) {
		return new Resource<>(veiculo, linkTo(methodOn(VeiculoController.class).buscarPorId(veiculo.getIdVeiculo())).withSelfRel());
	}

}
