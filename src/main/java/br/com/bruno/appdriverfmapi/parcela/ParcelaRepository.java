package br.com.bruno.appdriverfmapi.parcela;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.bruno.appdriverfmapi.custo.Custo;

@Repository
public interface ParcelaRepository extends JpaRepository<Parcela, Long> {
	public List<Parcela> findByCusto(Custo custo);
	public List<Parcela> findByPagoOrderByDataVencimentoAsc(boolean pago, Pageable pagina);
	public Long countByPago(boolean pago);
}
