package br.com.bruno.appdriverfmapi.parcela;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

@SuppressWarnings("serial")
public class ParcelasSerializer extends StdSerializer<List<Parcela>> {
	
	public ParcelasSerializer() {
		this(null);
	}

	protected ParcelasSerializer(Class<List<Parcela>> t) {
		super(t);
			}

	@Override
	public void serialize(List<Parcela> value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		List<Map<String, Object>> parcelas = new ArrayList<Map<String,Object>>();
		for(Parcela parcelaEntrada : value) {
			Map<String, Object> parcela = new HashMap<String, Object>();
			parcela.put("idParcela", parcelaEntrada.getIdParcela());
			parcela.put("dataVencimento", parcelaEntrada.getDataVencimento());
			parcela.put("valor", parcelaEntrada.getValor());
			parcela.put("pago", parcelaEntrada.isPago());
			parcelas.add(parcela);
		}
		gen.writeObject(parcelas);
		
	}

}
