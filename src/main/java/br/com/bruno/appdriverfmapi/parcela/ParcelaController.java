package br.com.bruno.appdriverfmapi.parcela;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class ParcelaController {
	
	@Autowired
	private ParcelaRepository repositorio;
	
	@Autowired
	private ParcelaResourceAssembler assembler;
	
	@GetMapping("/parcela/{numeroPagina}/{tamanhoPagina}")
	public Resources<Resource<Parcela>> listarEmAberto(@PathVariable int numeroPagina, @PathVariable int tamanhoPagina){
		Pageable pagina = PageRequest.of(numeroPagina, tamanhoPagina);
		List<Resource<Parcela>> parcelas = repositorio.findByPagoOrderByDataVencimentoAsc(false, pagina).stream()
				.map(parcela -> assembler.toResource(parcela)).collect(Collectors.toList());
		
		return new Resources<>(parcelas, linkTo(methodOn(ParcelaController.class).listarEmAberto(numeroPagina, tamanhoPagina)).withSelfRel());
	}
	
	@GetMapping("parcela")
	public Resource<Long> buscarContagemParcela(){
		Long contagemPaginas = repositorio.countByPago(false);
		return new Resource<>( contagemPaginas, linkTo( methodOn(ParcelaController.class).buscarContagemParcela()).withSelfRel() );
	}
	
	@GetMapping("/parcela/{idParcela}")
	public Resource<Parcela> buscar(@PathVariable Long idParcela){
		return repositorio.findById(idParcela)
				.map(parcela -> { return assembler.toResource(parcela); }).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}
	
	@PutMapping("/parcela")
	public ResponseEntity<?> atualizar(@RequestBody Parcela parcela){
		Resource<Parcela> corpoResposta = assembler.toResource(repositorio.save(parcela));
		return ResponseEntity.accepted().body(corpoResposta);
	}

}
