package br.com.bruno.appdriverfmapi.parcela;

import java.math.BigDecimal;
import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import br.com.bruno.appdriverfmapi.custo.Custo;

@Entity
@Table(name="parcela")
public class Parcela {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idParcela;
	
	@NotNull
	private BigDecimal valor;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(iso=ISO.DATE)
	@NotNull
	private Calendar dataVencimento;
	
	@ManyToOne
	@JoinColumn(name="idCusto")
	private Custo custo;
	
	private boolean pago;

	public Long getIdParcela() {
		return idParcela;
	}

	public Parcela setIdParcela(Long idParcela) {
		this.idParcela = idParcela;
		return this;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public Parcela setValor(BigDecimal valor) {
		this.valor = valor;
		return this;
	}

	public Calendar getDataVencimento() {
		return dataVencimento;
	}

	public Parcela setDataVencimento(Calendar dataVecimento) {
		this.dataVencimento = dataVecimento;
		return this;
	}

	public Custo getCusto() {
		return custo;
	}

	public Parcela setCusto(Custo custo) {
		this.custo = custo;
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Parcela) {
			if(this.getIdParcela() == ((Parcela) obj).getIdParcela()) {
				return true;
			}
		}
		return false;
	}

	public boolean isPago() {
		return pago;
	}

	public Parcela setPago(boolean pago) {
		this.pago = pago;
		return this;
	}
	
}
