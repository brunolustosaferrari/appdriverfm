package br.com.bruno.appdriverfmapi.parcela;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class ParcelaResourceAssembler implements ResourceAssembler<Parcela, Resource<Parcela>> {

	@Override
	public Resource<Parcela> toResource(Parcela entity) {
		return new Resource<>(entity, linkTo(methodOn(ParcelaController.class).buscar(entity.getIdParcela())).withSelfRel());
	}

}
