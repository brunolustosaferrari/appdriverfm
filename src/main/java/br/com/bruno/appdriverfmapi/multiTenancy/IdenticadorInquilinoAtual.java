package br.com.bruno.appdriverfmapi.multiTenancy;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.stereotype.Component;

import br.com.bruno.appdriverfmapi.gerenciadorBaseDados.ConstantesPredefinidas;

/**
 * Implementação da interface usada pelo hibernate para identificar em qual schema deve ser realizada a persistência de dados.
 * @author Bruno Lustosa Ferrari
 *
 */
@Component
public class IdenticadorInquilinoAtual implements CurrentTenantIdentifierResolver {

	@Override
	public String resolveCurrentTenantIdentifier() {
		if(ContextoInquilino.getInquilinoAtual() != null) {
			return ContextoInquilino.getInquilinoAtual();
		}
		
		return ConstantesPredefinidas.DEFAULT_SCHEMA;
	}

	@Override
	public boolean validateExistingCurrentSessions() {
		return false;
	}

}
