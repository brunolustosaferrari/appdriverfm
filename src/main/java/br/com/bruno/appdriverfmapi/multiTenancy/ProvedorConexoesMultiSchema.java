package br.com.bruno.appdriverfmapi.multiTenancy;

import java.sql.Connection;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.bruno.appdriverfmapi.gerenciadorBaseDados.ConnectionFactory;

/**
 * Implementação da interface do Hibernate para prover a conexão ao schema do tenant resolvido.
 * @author bruno
 *
 */
@SuppressWarnings("serial")
@Component
public class ProvedorConexoesMultiSchema implements MultiTenantConnectionProvider {
	
	@Autowired
	private DataSource dataSource;
	
	@Override
	public boolean isUnwrappableAs(@SuppressWarnings("rawtypes") Class unwrapType) {
		return false;
	}

	@Override
	public <T> T unwrap(Class<T> unwrapType) {
		return null;
	}

	@Override
	public Connection getAnyConnection() throws SQLException {
		
		return dataSource.getConnection();
	}

	@Override
	public void releaseAnyConnection(Connection connection) throws SQLException {
		connection.close();

	}

	@Override
	public Connection getConnection(String tenantIdentifier) throws SQLException {
		if(tenantIdentifier != null) {
			return ConnectionFactory.getConnection(tenantIdentifier);
		}else {
			return ConnectionFactory.getConnection();
		}
	}

	@Override
	public void releaseConnection(String tenantIdentifier, Connection connection) throws SQLException {
		connection.createStatement().execute("USE "+ tenantIdentifier);
		connection.close();

	}

	@Override
	public boolean supportsAggressiveRelease() {
		return false;
	}

}
