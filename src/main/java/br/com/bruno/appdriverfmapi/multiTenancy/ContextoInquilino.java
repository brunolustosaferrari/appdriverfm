package br.com.bruno.appdriverfmapi.multiTenancy;

/**
 * Classe responsável por amazenar o schema do usuário para persistência durante uma tread.
 * @author Bruno Lustosa Ferrari
 *
 */
public class ContextoInquilino {
	
	private static ThreadLocal<String> inquilinoAtual = new ThreadLocal<>();
	
	/**
	 * Define o schema para persistência 
	 * @param schemaAtual String com o nome do schema do tenant para persistência
	 */
	public static void setInquilinoAtual(String schemaAtual) {
		inquilinoAtual.set(schemaAtual);
	}
	
	/**
	 * Obtém o nome do schema do tenant na tread em que é invocado
	 * @return O nome do schema sendo utilizado
	 */
	public static String getInquilinoAtual() {
		return inquilinoAtual.get();
	}
	
	/**
	 * Exclui o schema definido para a tread.
	 */
	public static void limpar() {
		inquilinoAtual.set(null);
	}
	
}
