package br.com.bruno.appdriverfmapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppdriverfmapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppdriverfmapiApplication.class, args);
	}

}

