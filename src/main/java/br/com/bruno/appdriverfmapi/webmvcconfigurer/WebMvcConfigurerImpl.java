package br.com.bruno.appdriverfmapi.webmvcconfigurer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import br.com.bruno.appdriverfmapi.autenticacao.AutenticacaoInterceptor;

/**
 * Implementação da interface WebMvcConfigurer do Spring.
 *
 */
@Configuration
@EnableWebMvc
public class WebMvcConfigurerImpl implements WebMvcConfigurer {
	
	@Autowired
	AutenticacaoInterceptor autenticacaoInteceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(autenticacaoInteceptor);

	}
	
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
			.allowedOrigins("*")
			.allowedHeaders("*")
			.allowedMethods("*")
			.exposedHeaders("Access-Control-Allow-Origin", "Access-Control-Allow-Headers");
	}
	
}
