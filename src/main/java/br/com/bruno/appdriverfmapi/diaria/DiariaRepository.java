package br.com.bruno.appdriverfmapi.diaria;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DiariaRepository extends JpaRepository<Diaria, Long> {
	
	@Query(value="SELECT * FROM diaria ORDER BY dataHoraFim DESC LIMIT 7", nativeQuery=true)
	public List<Diaria> findUltimaSemana();
	
	public List<Diaria> findTop30ByOrderByDataHoraFimDesc();

}
