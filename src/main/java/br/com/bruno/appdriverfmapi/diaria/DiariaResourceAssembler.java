package br.com.bruno.appdriverfmapi.diaria;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class DiariaResourceAssembler implements ResourceAssembler<Diaria, Resource<Diaria>> {

	@Override
	public Resource<Diaria> toResource(Diaria diaria) {
		return new Resource<>(diaria, linkTo(methodOn(DiariaController.class).buscarPorId(diaria.getIdDiaria())).withSelfRel());
	}

}
