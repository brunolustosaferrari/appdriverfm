package br.com.bruno.appdriverfmapi.diaria;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.bruno.appdriverfmapi.rendimentoDiario.RendimentoDiario;
import br.com.bruno.appdriverfmapi.rendimentoDiario.RendimentoDiarioRepository;

@RestController 
public class DiariaController {
	
	@Autowired
	private DiariaRepository repositorio;
	
	@Autowired
	private RendimentoDiarioRepository repositorioRendimento;
	
	@Autowired
	private DiariaResourceAssembler assembler;
	
	@GetMapping("/diaria")
	public Resources<Resource<Diaria>> listar(){
		return this.listarPorPagina(0);
	}
	
	@GetMapping("/diaria/ultima-semana")
	public Resources<Resource<Diaria>> listarSemana(){
		List<Resource<Diaria>> listaDiarias = repositorio.findUltimaSemana().stream().map(diaria-> assembler.toResource(diaria)).collect(Collectors.toList());
		return new Resources<>(listaDiarias, linkTo(methodOn(DiariaController.class).listar()).withSelfRel());
	}
	
	@GetMapping("/diaria/ultimo-mes")
	public Resources<Resource<Diaria>> listarMes(){
		List<Resource<Diaria>> listaDiarias = repositorio.findTop30ByOrderByDataHoraFimDesc().stream().map(diaria-> assembler.toResource(diaria)).collect(Collectors.toList());
		return new Resources<>(listaDiarias, linkTo(methodOn(DiariaController.class).listar()).withSelfRel());
	}
	
	@GetMapping("/diaria/pagina")
	public Resource<Long> contagemPaginas(){
		Long contEntidades = new Long((long) Math.floor(repositorio.count() / 20));
		return new Resource<>(contEntidades);
	}
	
	@GetMapping("/diaria/pagina/{numero}")
	public Resources<Resource<Diaria>> listarPorPagina(@PathVariable int numero){
		Pageable pagina = PageRequest.of(numero, 20, Sort.by("dataHoraFim").descending());
		List<Resource<Diaria>> listaDiarias = repositorio.findAll(pagina).stream().map(diaria-> assembler.toResource(diaria)).collect(Collectors.toList());
		return new Resources<>(listaDiarias, linkTo(methodOn(DiariaController.class).listar()).withSelfRel());	}
	
	@GetMapping("/diaria/{id}")
	public Resource<Diaria> buscarPorId(@PathVariable Long id){
		return repositorio
				.findById(id)
				.map(diaria -> assembler.toResource(diaria))
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Diária não encontrada"));
	}
	
	@PostMapping("/diaria")
	@Transactional(propagation=Propagation.REQUIRED)
	public ResponseEntity<?> criar(@RequestBody @Valid Diaria diaria ){
		try {
			diaria = repositorio.save(diaria);
			for(RendimentoDiario rendimento : diaria.getRendimentosDiarios()) {
				rendimento.setDiaria(diaria);
				rendimento = repositorioRendimento.save(rendimento);
			}
			Resource<Diaria> corpoResposta = assembler.toResource(diaria);
			return ResponseEntity.created(new URI(corpoResposta.getId().expand().getHref())).body(corpoResposta);
		} catch (Exception e) {
			e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Ocorreu um erro ao gerar a resposta");
		}
	}
	
	@PutMapping("/diaria")
	@Transactional
	public ResponseEntity<?> atualizar(@RequestBody @Valid Diaria diaria){
		List<RendimentoDiario> rendimentosAtualizar = diaria.getRendimentosDiarios();
		diaria = repositorio.save(diaria);
		for(RendimentoDiario rendimento : rendimentosAtualizar ) {
			rendimento.setDiaria(diaria);
			rendimento = repositorioRendimento.save(rendimento);
		}
		List<RendimentoDiario> rendimentosAnteriores = repositorioRendimento.findByDiaria(diaria);
		for(RendimentoDiario rendimento : rendimentosAnteriores) {
			if(!rendimentosAtualizar.contains(rendimento)) {
				repositorioRendimento.delete(rendimento);
			}
		}
		Resource<Diaria> corpoResposta = assembler.toResource(diaria);
		return ResponseEntity.accepted().body(corpoResposta);
	}
	
	@DeleteMapping("/diaria/{id}")
	public ResponseEntity<?> excluir(@PathVariable Long id){
		try {
			repositorio.deleteById(id);
			return ResponseEntity.noContent().build();
		}catch(IllegalArgumentException ex) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Diaria não encontrada");
		}
	}
	
}
