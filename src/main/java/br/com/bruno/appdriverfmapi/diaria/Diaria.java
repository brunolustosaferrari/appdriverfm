package br.com.bruno.appdriverfmapi.diaria;

import java.util.Calendar;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import br.com.bruno.appdriverfmapi.rendimentoDiario.RendimentoDiario;

/**
 * Classe responsável por modelar um diária trabalhada pelo motorista em n pares aplicativo / veiculo
 * 
 */
@Entity
@Table(name="diaria")
public class Diaria {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idDiaria;
	
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(iso=ISO.DATE_TIME)
	@NotNull
	private Calendar dataHoraInicio;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(iso=ISO.DATE_TIME)
	@NotNull
	private Calendar dataHoraFim;
	
	@OneToMany(mappedBy="diaria", targetEntity=RendimentoDiario.class, fetch=FetchType.LAZY, orphanRemoval = true)
	private List<RendimentoDiario> rendimentosDiarios;
	
	public Long getIdDiaria() {
		return idDiaria;
	}
	public Diaria setIdDiaria(Long idDiaria) {
		this.idDiaria = idDiaria;
		return this;
	}
	
	public Calendar getDataHoraInicio() {
		return dataHoraInicio;
	}
	public Diaria setDataHoraInicio(Calendar dataHoraInicio) {
		this.dataHoraInicio = dataHoraInicio;
		return this;
	}
	public Calendar getDataHoraFim() {
		return dataHoraFim;
	}
	public Diaria setDataHoraFim(Calendar dataHoraFim) {
		this.dataHoraFim = dataHoraFim;
		return this;
	}
	
	public List<RendimentoDiario> getRendimentosDiarios() {
		return rendimentosDiarios;
	}
	
	public Diaria setRendimentosDiarios(List<RendimentoDiario> rendimentosDiario) {
		this.rendimentosDiarios = rendimentosDiario;
		return this;
	}
	
	public void addRendimentosDiarios(RendimentoDiario rendimentoDiario) {
		this.rendimentosDiarios.add(rendimentoDiario);
		rendimentoDiario.setDiaria(this);
	}
}
