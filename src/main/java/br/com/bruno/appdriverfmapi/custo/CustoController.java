package br.com.bruno.appdriverfmapi.custo;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.bruno.appdriverfmapi.parcela.Parcela;
import br.com.bruno.appdriverfmapi.parcela.ParcelaRepository;

@RestController
public class CustoController {
	
	@Autowired
	private CustoRepository repositorio;
	
	@Autowired
	private ParcelaRepository repositorioParcelas;
	
	@Autowired
	private CustoResourceAssembler assembler;
		
	@GetMapping("custo/pagina/{numero}")
	public Resources<Resource<Custo>> listarPorPagina(@PathVariable int numero){
		Pageable pagina = PageRequest.of(numero, 20, Sort.by("data").descending());
		List<Resource<Custo>> listaCustos = repositorio.findAll(pagina).stream().map(custo -> assembler.toResource(custo)).collect(Collectors.toList());
		return new Resources<>(listaCustos, linkTo( methodOn(CustoController.class).listarPorPagina(numero)).withSelfRel() );
	}
	
	@GetMapping("custo/pagina")
	public Resource<Long> buscarContagemPaginas(){
		Long contagemPaginas = (long) Math.floor( repositorio.count() / 20 );
		return new Resource<>( contagemPaginas, linkTo( methodOn(CustoController.class).buscarContagemPaginas()).withSelfRel() );
	}
	
	@GetMapping("custo")
	public Resources<Resource<Custo>> listar(){
		List<Resource<Custo>> listaCustos = repositorio.findAll(Sort.by("data").descending()).stream().map(custo -> assembler.toResource(custo)).collect(Collectors.toList());
		return new Resources<>(listaCustos, linkTo( methodOn(CustoController.class).listar()).withSelfRel() );
	}
	
	@GetMapping("custo/{idCusto}")
	public Resource<Custo> buscarPorId(@PathVariable Long idCusto){
		return repositorio.findById(idCusto).map(custo -> assembler.toResource(custo)).orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
	}
	
	@PostMapping("custo")
	@Transactional
	public ResponseEntity<?> criar(@RequestBody @Valid Custo custo){
		try {
			custo = repositorio.save(custo);
			List<Parcela> parcelas = custo.getParcelas();
			for(Parcela parcela : parcelas){
				parcela.setCusto(custo);
				repositorioParcelas.save(parcela);
			}
			Resource<Custo> corpoResposta = assembler.toResource(custo);
			return ResponseEntity.created( new URI(corpoResposta.getId().expand().getHref()) ).body(corpoResposta);
		} catch (URISyntaxException e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping("custo")
	@Transactional
	public ResponseEntity<?> atualizar(@RequestBody @Valid Custo custo){
		List<Parcela> parcelasAtuais = repositorioParcelas.findByCusto(custo);
		List<Parcela> parcelasNovas = custo.getParcelas();
		for(Parcela parcela : parcelasAtuais) {
			if(!parcelasNovas.contains(parcela)) {
				repositorioParcelas.delete(parcela);
			}
		}
		
		parcelasNovas.forEach(parcela->System.out.println(parcela.getValor()));
		for(Parcela parcela : parcelasNovas){
			parcela.setCusto(custo);
			repositorioParcelas.save(parcela);
		}
		Resource<Custo> corpoResposta = assembler.toResource(custo = repositorio.save(custo));
		return ResponseEntity.accepted().body(corpoResposta);
	}
	
	@DeleteMapping("custo/{idCusto}")
	public ResponseEntity<?> excluir(@PathVariable Long idCusto){
		try {
			Custo custo = repositorio.findById(idCusto)
							.orElseThrow(()-> new ResponseStatusException(HttpStatus.NOT_FOUND));
			custo.getParcelas().forEach(parcela -> repositorioParcelas.delete(parcela) );
			repositorio.delete(custo);
			return ResponseEntity.noContent().build();
		}catch(Exception e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		
	}
}
