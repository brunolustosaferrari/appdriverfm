package br.com.bruno.appdriverfmapi.custo;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class CustoResourceAssembler implements ResourceAssembler<Custo, Resource<Custo>> {

	@Override
	public Resource<Custo> toResource(Custo entity) {
		return new Resource<>(entity, linkTo( methodOn(CustoController.class).buscarPorId(entity.getIdCusto()) ).withSelfRel());
	}

}
