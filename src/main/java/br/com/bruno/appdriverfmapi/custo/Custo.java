package br.com.bruno.appdriverfmapi.custo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PostLoad;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.bruno.appdriverfmapi.parcela.Parcela;
import br.com.bruno.appdriverfmapi.parcela.ParcelasSerializer;
import br.com.bruno.appdriverfmapi.tipocusto.TipoCusto;
import br.com.bruno.appdriverfmapi.veiculo.Veiculo;

@Entity
@Table(name="custo")
public class Custo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idCusto;
	
	@Size(max=45)
	private String titulo;
	private String descricao;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(iso=ISO.DATE)
	private Calendar data;
	
	@Transient
	private BigDecimal valor;
	
	@ManyToOne
	@JoinColumn(name="idTipoCusto")
	@NotNull
	private TipoCusto tipo;
	
	@OneToMany(mappedBy="custo", targetEntity=Parcela.class, fetch=FetchType.LAZY,orphanRemoval=true)
	@JsonSerialize(using=ParcelasSerializer.class)
	private List<Parcela> parcelas;
	
	@ManyToOne
	@JoinColumn(name="idVeiculo")
	private Veiculo veiculo;
	
	public Long getIdCusto() {
		return idCusto;
	}

	public Custo setIdCusto(Long idCusto) {
		this.idCusto = idCusto;
		return this;
	}

	public String getTitulo() {
		return titulo;
	}

	public Custo setTitulo(String titulo) {
		this.titulo = titulo;
		return this;
	}

	public String getDescricao() {
		return descricao;
	}

	public Custo setDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}

	public Calendar getData() {
		return data;
	}

	public Custo setData(Calendar data) {
		this.data = data;
		return this;
	}

	public BigDecimal getValor() {
		if( this.valor == null ) this.valor = new BigDecimal(0);
		if( (this.valor.equals(new BigDecimal(0)) || this.valor.equals(null)) && this.parcelas != null ) this.setValor();
		return valor;
	}
	
	@PostLoad
	public void setValor() {
		this.valor = new BigDecimal(0);
		if(this.parcelas != null) {
			for(Parcela parcela:this.parcelas) {
				if(parcela.getValor() != null) this.valor.add(parcela.getValor());
			}
		}
	}

	public TipoCusto getTipo() {
		return tipo;
	}

	public Custo setTipo(TipoCusto tipo) {
		this.tipo = tipo;
		return this;
	}

	public List<Parcela> getParcelas() {
		return parcelas;
	}

	public Custo setParcelas(List<Parcela> parcelas) {
		for( Parcela parcela : parcelas ) {
			this.addParcela(parcela);
		}
		return this;
	}
	
	public Custo addParcela(Parcela parcela) {
		if(this.parcelas == null) {
			this.parcelas = new ArrayList<Parcela>();
		}
		this.parcelas.add(parcela);
		parcela.setCusto(this);
		return this;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public Custo setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
		return this;
	}
	
	public Custo removeParcela(Parcela parcela) {
		this.parcelas.remove(parcela);
		return this;
	}
	
}
