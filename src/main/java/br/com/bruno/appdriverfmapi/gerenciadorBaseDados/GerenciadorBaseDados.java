package br.com.bruno.appdriverfmapi.gerenciadorBaseDados;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.transaction.annotation.Transactional;

import br.com.bruno.appdriverfmapi.usuario.Usuario;

/**
 * Classe responsável pelo gerenciamento das bases de Usuários
 * @author Bruno Lustosa Ferrari
 *
 */
public class GerenciadorBaseDados {
	
	private Usuario proprietario;
	
	private Connection conexao;
	
	public GerenciadorBaseDados() {
		try {
			this.conexao = ConnectionFactory.getConnection();
		}catch(RuntimeException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Cria um gerenciador para a base do Usuário
	 * @param usuario Proprietário da base de dados
	 */
	
	public GerenciadorBaseDados(Usuario usuario) {
		this();
		this.proprietario = usuario;
	}
	
	/**
	 * Cria uma nova base de dados para o usuário passado no construtor. O processamento é transacional.
	 * @throws RuntimeException Quando ocorre qualquer erro no processo
	 */
	@Transactional
	public void criarBase() throws RuntimeException{
		try {
			
			this.criarSchema();
			this.criarTabelaVeiculo();
			this.criarTabelaAplicativo();
			this.criarTabelaDiaria();
			this.criarTabelaRendimentoDiario();
			this.criarTabelaTipoCusto();
			this.criarTabelaCusto();
			this.criarTabelaParcela();
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
	}
	
	private void criarSchema() throws Exception {
		
		if(this.proprietario == null || this.proprietario.getIdUsuario() == null) {
			throw new Exception("Usuário não informado");
		}
		
		String sqlCriarSchema = "CREATE SCHEMA IF NOT EXISTS `" +
				proprietario.getNomeSchema()
				+ "` DEFAULT CHARACTER SET utf8 ";
		
		Statement statementCriarSchema = this.conexao.createStatement();
		statementCriarSchema.executeUpdate(sqlCriarSchema);

	}
	
	private void criarTabelaVeiculo() throws Exception {
		String sqlCriarTabela = "CREATE TABLE IF NOT EXISTS `"+  proprietario.getNomeSchema() + "`.`veiculo` (\n" + 
								"  `idVeiculo` INT NOT NULL AUTO_INCREMENT,\n" + 
								"  `nome` VARCHAR(45) NULL,\n" + 
								"  `descricao` TEXT NULL,\n" + 
								"  `placa` VARCHAR(8) NULL,\n" + 
								"  `ativo` TINYINT(1) NULL DEFAULT 1,\n" +
								"  PRIMARY KEY (`idVeiculo`))" + 
								"  ENGINE = InnoDB";
		
		Statement statementCriarTabela = this.conexao.createStatement();
		statementCriarTabela.executeUpdate(sqlCriarTabela);
	}
	
	private void criarTabelaAplicativo() throws Exception {
		
		String sqlCriarTabela = "CREATE TABLE IF NOT EXISTS `"+  proprietario.getNomeSchema() + "`.`aplicativo` (\n" + 
								"  `idAplicativo` INT NOT NULL AUTO_INCREMENT,\n" + 
								"  `nome` VARCHAR(45) NULL,\n" + 
								"  `descricao` TEXT NULL,\n" + 
								"  `ativo` TINYINT(1) NULL DEFAULT 1,\n" +
								"  PRIMARY KEY (`idAplicativo`),\n" + 
								"  UNIQUE INDEX `nome_UNIQUE` (`nome` ASC))" +
								"  ENGINE = InnoDB ";
						
		Statement statementCriarTabela = this.conexao.createStatement();
		statementCriarTabela.executeUpdate(sqlCriarTabela);
	}
	
	private void criarTabelaDiaria() throws SQLException {
		
		String sqlCriarTabela = "CREATE TABLE IF NOT EXISTS `"+  proprietario.getNomeSchema() + "`.`diaria` (\n" + 
								"  `idDiaria` INT NOT NULL AUTO_INCREMENT,\n" + 
								"  `dataHoraInicio` TIMESTAMP NULL,\n" + 
								"  `dataHoraFim` TIMESTAMP NULL,\n" + 
								"  PRIMARY KEY (`idDiaria`))\n" + 
								"ENGINE = InnoDB";	
		
		Statement statementCriarTabela = this.conexao.createStatement();
		statementCriarTabela.executeUpdate(sqlCriarTabela);		
	}
	
	private void criarTabelaRendimentoDiario() throws SQLException {
		
		String sqlCriarTabela = "CREATE TABLE IF NOT EXISTS `"+  proprietario.getNomeSchema() +"`.`rendimentoDiario` (\n" + 
								"  `idRendimentoDiario` INT NOT NULL AUTO_INCREMENT,\n" + 
								"  `tempoEmCorrida` TIME NULL,\n" + 
								"  `distanciaPercorridaEmKm` FLOAT NULL,\n" + 
								"  `valorRecebido` DOUBLE NOT NULL,\n" + 
								"  `idVeiculo` INT NOT NULL,\n" + 
								"  `idAplicativo` INT NOT NULL,\n" + 
								"  `idDiaria` INT NULL,\n" + 
								"  PRIMARY KEY (`idRendimentoDiario`, `idVeiculo`, `idAplicativo`),\n" + 
								"  INDEX `fk_rendimentoDiario_veiculo_idx` (`idVeiculo` ASC),\n" + 
								"  INDEX `fk_rendimentoDiario_aplicativo1_idx` (`idAplicativo` ASC),\n" + 
								"  INDEX `fk_rendimentoDiario_diaria1_idx` (`idDiaria` ASC),\n" + 
								"  CONSTRAINT `fk_rendimentoDiario_veiculo`\n" + 
								"    FOREIGN KEY (`idVeiculo`)\n" + 
								"    REFERENCES `"+  proprietario.getNomeSchema() +"`.`veiculo` (`idVeiculo`)\n" + 
								"    ON DELETE RESTRICT\n" + 
								"    ON UPDATE CASCADE,\n" + 
								"  CONSTRAINT `fk_rendimentoDiario_aplicativo1`\n" + 
								"    FOREIGN KEY (`idAplicativo`)\n" + 
								"    REFERENCES `"+  proprietario.getNomeSchema() +"`.`aplicativo` (`idAplicativo`)\n" + 
								"    ON DELETE RESTRICT\n" + 
								"    ON UPDATE CASCADE,\n" + 
								"  CONSTRAINT `fk_rendimentoDiario_diaria1`\n" + 
								"    FOREIGN KEY (`idDiaria`)\n" + 
								"    REFERENCES `"+  proprietario.getNomeSchema() +"`.`diaria` (`idDiaria`)\n" + 
								"    ON DELETE RESTRICT\n" + 
								"    ON UPDATE CASCADE)\n" + 
								"ENGINE = InnoDB";	
		
		Statement statementCriarTabela = this.conexao.createStatement();
		statementCriarTabela.executeUpdate(sqlCriarTabela);		
	}
	
	private void criarTabelaTipoCusto() throws SQLException {
		
		String sqlCriarTabela = "CREATE TABLE IF NOT EXISTS `"+  proprietario.getNomeSchema() +"`.`tipoCusto` (\n" + 
								"  `idTipoCusto` INT NOT NULL AUTO_INCREMENT,\n" + 
								"  `descricao` VARCHAR(45) NULL,\n" + 
								"  PRIMARY KEY (`idTipoCusto`))\n" + 
								"ENGINE = InnoDB";	
						
		Statement statementCriarTabela = this.conexao.createStatement();
		statementCriarTabela.executeUpdate(sqlCriarTabela);		
	}
	
	private void criarTabelaCusto() throws SQLException {
		
		String sqlCriarTabela = "CREATE TABLE IF NOT EXISTS `"+  proprietario.getNomeSchema() +"`.`custo` (\n" + 
								"  `idCusto` INT NOT NULL AUTO_INCREMENT,\n" + 
								"  `data` TIMESTAMP NOT NULL,\n" + 
								"  `titulo` VARCHAR(45) NULL,\n" + 
								"  `descricao` TEXT NULL,\n" + 
								"  `idTipoCusto` INT NOT NULL,\n" + 
								"  `idVeiculo` INT NOT NULL,\n" + 
								"  PRIMARY KEY (`idCusto`),\n" + 
								"  INDEX `fk_custo_tipoCusto1_idx` (`idTipoCusto` ASC),\n" + 
								"  INDEX `fk_custo_veiculo1_idx` (`idVeiculo` ASC),\n" + 
								"  CONSTRAINT `fk_custo_tipoCusto1`\n" + 
								"    FOREIGN KEY (`idTipoCusto`)\n" + 
								"    REFERENCES `"+  proprietario.getNomeSchema() +"`.`tipoCusto` (`idTipoCusto`)\n" + 
								"    ON DELETE RESTRICT\n" + 
								"    ON UPDATE CASCADE,\n" + 
								"  CONSTRAINT `fk_custo_veiculo1`\n" + 
								"    FOREIGN KEY (`idVeiculo`)\n" + 
								"    REFERENCES `"+  proprietario.getNomeSchema() +"`.`veiculo` (`idVeiculo`)\n" + 
								"    ON DELETE RESTRICT\n" + 
								"    ON UPDATE CASCADE)\n" + 
								"ENGINE = InnoDB";	
						
		Statement statementCriarTabela = this.conexao.createStatement();
		statementCriarTabela.executeUpdate(sqlCriarTabela);		
	}
	
	private void criarTabelaParcela() throws SQLException {
		
		String sqlCriarTabela = "CREATE TABLE IF NOT EXISTS `"+  proprietario.getNomeSchema() +"`.`parcela` (\n" + 
								"  `idParcela` INT NOT NULL AUTO_INCREMENT,\n" + 
								"  `dataVencimento` DATE NULL,\n" + 
								"  `valor` DOUBLE NULL,\n"+
								"  `pago` TINYINT(1) NULL DEFAULT 0,\n" + 
								"  `idCusto` INT NOT NULL,\n" + 
								"  PRIMARY KEY (`idParcela`),\n" + 
								"  INDEX `fk_parcela_custo1_idx` (`idCusto` ASC),\n" + 
								"  CONSTRAINT `fk_parcela_custo1`\n" + 
								"    FOREIGN KEY (`idCusto`)\n" + 
								"    REFERENCES `"+  proprietario.getNomeSchema() +"`.`custo` (`idCusto`)\n" + 
								"    ON DELETE RESTRICT\n" + 
								"    ON UPDATE CASCADE)\n" + 
								"ENGINE = InnoDB";	
						
		Statement statementCriarTabela = this.conexao.createStatement();
		statementCriarTabela.executeUpdate(sqlCriarTabela);		
	}
	
	@Override
	protected void finalize() throws Throwable {
		conexao.close();
		super.finalize();
	}
}
