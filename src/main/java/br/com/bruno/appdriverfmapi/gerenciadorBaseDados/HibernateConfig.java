package br.com.bruno.appdriverfmapi.gerenciadorBaseDados;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.hibernate.MultiTenancyStrategy;
import org.hibernate.cfg.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

import br.com.bruno.appdriverfmapi.multiTenancy.IdenticadorInquilinoAtual;
import br.com.bruno.appdriverfmapi.multiTenancy.ProvedorConexoesMultiSchema;

/**
 * Classe responsável por configurar o provedor de ORM.
 *   
 */
@Configuration
public class HibernateConfig {
	
	@Autowired
	private JpaProperties jpaProperties;
	
	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		return new HibernateJpaVendorAdapter();
				
	}
	
	/**
	 * Método responsável por prover um EntityManagerFactory para injeção, configurado para uma arquitetura de entrega multi-tenant.
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(DataSource dataSource,
			IdenticadorInquilinoAtual identificadorInquilino, ProvedorConexoesMultiSchema provedorConexao) {
		
		Map<String, Object> properties = new HashMap<>();
		properties.put(Environment.MULTI_TENANT, MultiTenancyStrategy.SCHEMA);
		properties.put(Environment.MULTI_TENANT_CONNECTION_PROVIDER, provedorConexao);
		properties.put(Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, identificadorInquilino);
		properties.putAll(jpaProperties.getProperties());
		
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(dataSource);
        em.setPackagesToScan("br.com.bruno.appdriverfmapi");
        em.setJpaVendorAdapter(jpaVendorAdapter());
        em.setJpaPropertyMap(properties);
        
        return em;
	}
}
