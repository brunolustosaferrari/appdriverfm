package br.com.bruno.appdriverfmapi.gerenciadorBaseDados;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 * Implementação do padrão factory, para obter conexão direta com a base de dados
 * @author Bruno Lustosa Ferrari
 *
 */
public abstract class ConnectionFactory {
	
    public static Connection getConnection(String nomeBase){
        try{
        	Class.forName("com.mysql.cj.jdbc.Driver");
            Connection conexaoRetorno =  DriverManager.getConnection("jdbc:mysql://localhost:3306/" + nomeBase, "root", "root");
            return conexaoRetorno;
        }catch(SQLException e){
            throw new RuntimeException(e);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
		}
    }
    
    public static Connection getConnection(){
    	return ConnectionFactory.getConnection("appdriverfm_db");
    }
}
