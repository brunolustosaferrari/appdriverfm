package br.com.bruno.appdriverfmapi.usuario;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.bruno.appdriverfmapi.gerenciadorBaseDados.GerenciadorBaseDados;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

@RestController
public class UsuarioController {
	
	@Autowired
	private UsuarioRepository repositorio;
	
	@Autowired
	private UsuarioResourceAssembler assembler;
	
	@GetMapping("/usuario")
	public Resources<Resource<Usuario>> listar(){
		List<Resource<Usuario>> usuarios = 
				repositorio.findAll().stream()
				.map(usuario -> assembler.toResource(usuario)).collect(Collectors.toList());
		
		return new Resources<>(usuarios,
				linkTo(methodOn(UsuarioController.class).listar()).withSelfRel());
		
		
	}
	
	@GetMapping("/usuario/{id}")
	public Resource<Usuario> buscarPorId(@PathVariable Long id){
		return repositorio.findById(id)
				.map( usuario -> assembler.toResource(usuario) )
				.orElseThrow( () -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario não encontrado"));
		
	}
	
	@PostMapping
	@Transactional
	public ResponseEntity<?> criar(@RequestBody @Valid Usuario usuario){
		try {
			if( repositorio.findByEmail(usuario.getEmail()) == null ) {
				usuario.setSenha(Utilitario.gerarHash256(usuario.getSenha()));
				usuario.setAutoridade(UsuarioAutoridade.MOTORISTA);
				Resource<Usuario> recursoUsuario = assembler.toResource(repositorio.save(usuario));
				GerenciadorBaseDados gerenciadorBd = new GerenciadorBaseDados(usuario);
				gerenciadorBd.criarBase();
				return ResponseEntity
							.created(new URI(recursoUsuario.getId().expand().getHref()))
							.body(recursoUsuario);
			}else {
				throw new ResponseStatusException(HttpStatus.CONFLICT, "Usuário já cadastrado, use um email válido");
			}
		}catch(NoSuchAlgorithmException ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Ocorreu um erro interno ao gravar o usuário, contate o administrador do serviço");
		}catch(URISyntaxException ex){
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Ocorreu um erro interno ao gravar o usuário, contate o administrador do serviço");
		}
		
	}
	
	@PutMapping("/usuario/{id}")
	public ResponseEntity<?> atualizar(@RequestBody @Valid Usuario novoUsuario, @PathVariable Long id){
		return repositorio.findById(id)
			.map(usuario -> {
				try {
					usuario.setEmail(novoUsuario.getEmail());
					usuario.setSenha(Utilitario.gerarHash256(novoUsuario.getSenha()));
					Resource<Usuario> recursoUsuario = assembler.toResource(repositorio.save(usuario));
					return ResponseEntity
							.accepted()
							.body(recursoUsuario);
				}catch(NoSuchAlgorithmException ex) {
					throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Ocorreu um erro interno ao gravar o usuário, contate o administrador do serviço");
				}
			})
			.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario não encontrado"));
	}
	
	@DeleteMapping("/usuario/{id}")
	public ResponseEntity<?> excluir(@PathVariable Long id){
		return repositorio.findById(id)
				.map(usuario -> {
					repositorio.delete(usuario);
					return ResponseEntity.noContent().build();	
				})
				.orElseThrow(() ->  new ResponseStatusException(HttpStatus.NOT_FOUND, "Usuario não encontrado"));
	}
}
