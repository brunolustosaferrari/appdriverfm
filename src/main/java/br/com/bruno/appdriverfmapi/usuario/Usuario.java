package br.com.bruno.appdriverfmapi.usuario;


import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;


@Entity
@Table(name="usuario")
public class Usuario {
		
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idUsuario;
	
	@NotEmpty
	@NotNull
	@Size(max=320, message="Um email não deve possuir mais de 320 caracteres")
	private String email;
	
	@NotEmpty
	@NotNull
	@Size(min = 6, message="A senha deve possuir ao mínimo 6 caracteres")
	private String senha;
	
	@Enumerated(EnumType.STRING)
	private UsuarioAutoridade autoridade;
	
	public Usuario setIdUsuario(Long idEntrada) {
		this.idUsuario = idEntrada;
		return this;
	}
	
	public Long getIdUsuario() {
		return this.idUsuario;
	}
	
	public Usuario setEmail(String emailEntrada) {
		this.email = emailEntrada;
		return this;
	}
	
	public String getEmail() {
		return this.email;
	}
	
	@JsonProperty
	public Usuario setSenha(String senhaHash) {
		this.senha = senhaHash;
		return this;
	}
	
	@JsonIgnore
	public String getSenha() {
		return this.senha;
	}
	
	@JsonProperty
	public Usuario setAutoridade(UsuarioAutoridade autoridadeEntrada) {
		this.autoridade = autoridadeEntrada;
		return this;
	}
	
	@JsonIgnore
	public UsuarioAutoridade getAutoridade() {
		return this.autoridade;
	}
	
	/**
	 * Método que gera o nome do schema para o usuário do contexto em que é chamado.
	 * @return Uma string com o nome do schema - ou null, caso o usuário tenha id == null.
	 */
	@JsonIgnore
	public String getNomeSchema() {
		if(this.idUsuario == null) return null;
		return "appdriverfm_user" +	(this.getIdUsuario().toString()).concat("_db");
	}
	
}
