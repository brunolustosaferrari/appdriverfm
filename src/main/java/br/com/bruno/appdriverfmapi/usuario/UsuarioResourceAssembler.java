package br.com.bruno.appdriverfmapi.usuario;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

/**
 * Classe responsável por gerar o estado representacional de um Usuário.
 * 
 */
@Component
public class UsuarioResourceAssembler implements ResourceAssembler<Usuario, Resource<Usuario>> {
	
	/**
	 * Cria um recurso do Spring HATEOAS para o usuário.
	 * @param usuario Usuário a ser representado
	 * @return Objeto do Spring para construção do corpo da resposta Http, contendo a representação do usuário
	 */
	@Override
	public Resource<Usuario> toResource(Usuario usuario){
		return new Resource<>(usuario,
				linkTo(methodOn(UsuarioController.class).buscarPorId(usuario.getIdUsuario())).withSelfRel());
	}
}
