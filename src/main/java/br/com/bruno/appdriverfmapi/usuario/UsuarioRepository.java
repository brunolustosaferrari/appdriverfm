package br.com.bruno.appdriverfmapi.usuario;

import org.springframework.data.jpa.repository.JpaRepository;
/**
 * Repositório para Usuario.
 * 
 */
public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
	
	public Usuario findByEmailAndSenha(String email, String senhaHash);
	public Usuario findByEmail(String email);
	
}
