package br.com.bruno.appdriverfmapi.aplicativo;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class AplicativoController {
	
	@Autowired
	AplicativoRepository repositorio;
	@Autowired
	AplicativoResourceAssembler assembler;

	@GetMapping("/aplicativo")
	public Resources<Resource<Aplicativo>> listar(){
		List<Resource<Aplicativo>> listaRecursos = repositorio.findByAtivo(true)
				.stream().map(aplicativo -> assembler.toResource(aplicativo)).collect(Collectors.toList());
		
		return new Resources<>(listaRecursos);
	}
	
	@GetMapping("/aplicativo/{idAplicativo}")
	public Resource<Aplicativo> buscarPorId(@PathVariable Long idAplicativo){
		return repositorio.findById(idAplicativo)
				.map(aplicativo-> assembler.toResource(aplicativo))
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Aplicativo não encontrado"));
	}
	
	@PostMapping("/aplicativo")
	public ResponseEntity<?> criar(@RequestBody @Valid Aplicativo aplicativo){
		if (repositorio.findByNomeAndAtivo(aplicativo.getNome(), true) == null) {
			Resource<Aplicativo> corpoResposta = assembler.toResource(repositorio.save(aplicativo));
			try {
				return ResponseEntity.created(new URI(corpoResposta.getId().expand().getHref()) ).body(corpoResposta);
			} catch (URISyntaxException e) {
				throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Erro ao processar o retorno");
			}
		}else {
			throw new ResponseStatusException(HttpStatus.CONFLICT, "O aplicativo já foi cadastrado.");
		}
	}
	
	@PutMapping("/aplicativo")
	public ResponseEntity<?> atualizar(@RequestBody @Valid Aplicativo aplicativo){
		Aplicativo verificacao = repositorio.findByNomeAndAtivo(aplicativo.getNome(), true);
		if ( verificacao != null ) {
			if( verificacao.getIdAplicativo() != aplicativo.getIdAplicativo() ) {
				throw new ResponseStatusException(HttpStatus.CONFLICT, "Já existe aplicativo com esse nome.");
			}
		}
		
		Resource<Aplicativo> corpoResposta = assembler.toResource(repositorio.save(aplicativo));
		return ResponseEntity.accepted().body(corpoResposta);
	}
	
	@DeleteMapping("/aplicativo/{idAplicativo}")
	public ResponseEntity<?> excluir(@PathVariable Long idAplicativo){
		try {
			repositorio.findById(idAplicativo)
				.map(aplicativo -> repositorio.save( aplicativo.setAtivo(false) ));
			return ResponseEntity.noContent().build();
		}catch(IllegalArgumentException ex) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Aplicativo não encontrado");
		}
	}
	

}
