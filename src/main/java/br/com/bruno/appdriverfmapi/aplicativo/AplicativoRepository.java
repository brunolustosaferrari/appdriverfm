package br.com.bruno.appdriverfmapi.aplicativo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Repositório para persistência de Aplicativo {@see br.com.bruno.appdriverfmapi.aplicativo}
 * 
 */
public interface AplicativoRepository extends JpaRepository<Aplicativo, Long> {

	public Aplicativo findByNome(String nome);
	public Aplicativo findByNomeAndAtivo(String nome, boolean ativo);
	public List<Aplicativo> findByAtivo(boolean ativo);
	
}
