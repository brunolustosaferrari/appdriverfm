package br.com.bruno.appdriverfmapi.aplicativo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * Classe reponsável por modelar um aplicativo
 * 
 */

@Entity
@Table(name="aplicativo")
public class Aplicativo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idAplicativo;
	@Size(max=45)
	@NotBlank
	private String nome;
	private String descricao;
	
	@JsonIgnore
	private boolean ativo = true;
	
	public Long getIdAplicativo() {
		return idAplicativo;
	}
	
	public Aplicativo setIdAplicativo(Long idAplicativo) {
		this.idAplicativo = idAplicativo;
		return this;
	}
	
	public String getNome() {
		return nome;
	}
	/**
	 * 
	 * @param nome Não pode estar em branco; 
	 * @return O objeto com o nome;
	 */
	public Aplicativo setNome(String nome) {
		this.nome = nome;
		return this;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public Aplicativo setDescricao(String descricao) {
		this.descricao = descricao;
		return this;
	}
	
	public boolean getAtivo() {
		return this.ativo;
	}
	
	public Aplicativo setAtivo(boolean ativo) {
		this.ativo = ativo;
		return this;
	}
	
	
}
