package br.com.bruno.appdriverfmapi.aplicativo;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;
/**
 * Classe responsável por gerar o estado representacional de um Aplicativo
 * @author bruno
 *
 */
@Component
public class AplicativoResourceAssembler implements ResourceAssembler<Aplicativo, Resource<Aplicativo>> {

	@Override
	public Resource<Aplicativo> toResource(Aplicativo aplicativo) {
		return new Resource<>(aplicativo, linkTo(methodOn(AplicativoController.class).buscarPorId(aplicativo.getIdAplicativo())). withSelfRel());
	}

}
