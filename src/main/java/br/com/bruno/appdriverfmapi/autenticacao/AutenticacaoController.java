package br.com.bruno.appdriverfmapi.autenticacao;

import java.security.NoSuchAlgorithmException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.auth0.jwt.exceptions.JWTCreationException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioRepository;
import br.com.bruno.appdriverfmapi.utilitario.Utilitario;

@RestController
public class AutenticacaoController {
	
	@Autowired
	UsuarioRepository repositorioUsuario;
	
	@Autowired
	ObjectMapper mapper;
	
	@GetMapping("/autenticacao/{email}/{senha}")
	public ResponseEntity<?> autenticar(@PathVariable String email, @PathVariable String senha) throws NullPointerException, JWTCreationException, NoSuchAlgorithmException{
		try {
			Usuario usuarioRequisicao = repositorioUsuario.findByEmailAndSenha(email, Utilitario.gerarHash256(senha));
			
			if(usuarioRequisicao != null) {
				String tokenAutorizacao =  AutenticacaoServicos.gerarToken(usuarioRequisicao);
				return ResponseEntity.accepted().contentType(MediaType.APPLICATION_JSON_UTF8).body(mapper.writeValueAsString(tokenAutorizacao)); 
			}else {
				throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, "Acesso negado.");
			}
		}catch(JsonProcessingException ex) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Erro ao processar token de acesso.");
			
		}
		
	}
}
