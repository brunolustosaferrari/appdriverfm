package br.com.bruno.appdriverfmapi.autenticacao;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import br.com.bruno.appdriverfmapi.multiTenancy.ContextoInquilino;
import br.com.bruno.appdriverfmapi.usuario.Usuario;
import br.com.bruno.appdriverfmapi.usuario.UsuarioAutoridade;
import br.com.bruno.appdriverfmapi.usuario.UsuarioRepository;
/**
 * Interceptador para requisições ao servidor.
 * 
 * @author Bruno Lustosa Ferrari
 *
 */
@Component
public class AutenticacaoInterceptor implements HandlerInterceptor {
	
	@Autowired
	UsuarioRepository repositorioUsuario;
	/**
	 * Verifica se o cliente está tentando acessar um recurso público - 
	 * {@link br.com.bruno.appdriverfmapi.autenticacao.AutenticacaoController} por get ou {@link br.com.bruno.appdriverfmapi.usuario.UsuarioController } 
	 * por post;
	 * Caso contrário procura pelo Header "authorization" no request para autenticar o usuário e resolver o tenant para o hibernate. 
	 * @throws Exception caso o cliente esteja tentando acessar um recurso que não o pertença ou o token não possa ser validado.
	 * @throws TokenExpiredException caso o token tenha expirado
	 */
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
//		ContextoInquilino.limpar();

		if( !request.getRequestURI().startsWith("/autenticacao") && !request.getMethod().equals("OPTIONS") ) {
			
			if( request.getRequestURI().startsWith("/usuario") && request.getMethod().equals("POST") ) {
				return true;
			}
								
			try {
				
				String tokenAutenticacao = request.getHeader("Authorization");
				DecodedJWT tokenDecodificado = AutenticacaoServicos.validarToken(tokenAutenticacao);
				Claim idUsarioClaim = tokenDecodificado.getClaim("idUsuario");
				
				Usuario usuarioRequisicao = repositorioUsuario
						.findById( Long.valueOf(idUsarioClaim.asString()) )
						.map(usuario -> { return usuario; })
						.orElseThrow(() -> new Exception());
				
				if( !request.getRequestURI().startsWith("/usuario") ) {
					ContextoInquilino.setInquilinoAtual(usuarioRequisicao.getNomeSchema());
					return true;
					
				}else {
					//Verifica se o cliente está acessando um recurso do próprio usuário na base de autenticação
					if( request.getMethod().equals("POST") || usuarioRequisicao.getAutoridade().equals(UsuarioAutoridade.ADMIN)
							|| (  request.getRequestURI().equals( "/usuario/" + usuarioRequisicao.getIdUsuario().toString())  
							&& ( request.getMethod().equals("GET") || request.getMethod().equals("DELETE") || request.getMethod().equals("PUT") ) ) ){
						return true;
					}else {
						throw new Exception();
					}
					
				}
				
			}catch( TokenExpiredException ex) {
				response.sendError(401, "Token expirado");
				return false;
			}catch( Exception ex ) {
				ex.printStackTrace();
				response.sendError(401, "Acesso negado");
				return false;
			}
		
		}
		
		return true;
	}
	
	/**
	 * Remove o tenant da tread
	 */
	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		ContextoInquilino.limpar();
	}
	
}
