package br.com.bruno.appdriverfmapi.autenticacao;

import java.util.Calendar;
import java.util.Date;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import br.com.bruno.appdriverfmapi.usuario.Usuario;
/**
 * Classe abstrata responsável por prover serviços para a autenticação
 * 
 * @author Bruno Lustosa Ferrari
 *
 */
public abstract class AutenticacaoServicos {
	
	private static final String chaveAutenticacao = "#1Fq<E].Uf>G";
	
	
	/**
	 * Gera um token JWT para autenticação do usuário
	 * @param usuario Usuário que está tentando autenticar
	 * @return String com um token JWT
	 * @throws NullPointerException Quando o usuário não tem id definido o método infere que o mesmo não existe na base
	 * @throws JWTCreationException Erro na geração do token
	 */
	public static String gerarToken(Usuario usuario) throws NullPointerException, JWTCreationException {
		if(usuario.getIdUsuario() == null) {
			throw new NullPointerException("Usuario não cadastrado");
		}
		
		Calendar dataExpiracao = Calendar.getInstance();
		dataExpiracao.add(Calendar.DAY_OF_MONTH, 7);
		Date dataToken = dataExpiracao.getTime();
		return JWT.create()
				.withSubject(usuario.getEmail())
				.withClaim("autoridade", usuario.getAutoridade().toString())
				.withClaim("idUsuario", usuario.getIdUsuario().toString())
				.withExpiresAt(dataToken)
				.sign(Algorithm.HMAC512(chaveAutenticacao));
	}
	
	/**
	 * Verifica a assinatura do token de autenticação
	 * @param tokenAutenticacao Token para ser verificado
	 * @return Retorna um objeto DecodedJWT a partir do qual o usuario será identificado a partir dos claims (idUsuario e autoridade)
	 */
	public static DecodedJWT validarToken(String tokenAutenticacao) {
		JWTVerifier verificador = JWT.require(Algorithm.HMAC512(chaveAutenticacao)).build();
		return verificador.verify(tokenAutenticacao);
		
	}
	
	
}
