/**
 * 
 */
package br.com.bruno.appdriverfmapi.rendimentoDiario;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import br.com.bruno.appdriverfmapi.diaria.Diaria;

/**
 * Repositório responsável pela persistência de um rendimento diário.
 * @author bruno
 *
 */
@Repository
public interface RendimentoDiarioRepository extends JpaRepository<RendimentoDiario, Long> {
	
	public List<RendimentoDiario> findByDiaria(Diaria diaria);

}
