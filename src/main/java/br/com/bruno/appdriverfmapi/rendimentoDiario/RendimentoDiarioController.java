package br.com.bruno.appdriverfmapi.rendimentoDiario;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class RendimentoDiarioController {

	@Autowired
	private RendimentoDiarioRepository repositorio;
	
	@Autowired
	private RendimentoDiarioResourceAssembler assembler;
	
	@GetMapping("/rendimento-diario/{id}")
	public Resource<RendimentoDiario> buscarPorId(@PathVariable Long id){
		try {
			return repositorio
					.findById(id)
					.map(rendimento -> assembler.toResource(rendimento))
					.orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Rendimento não encontrado"));
		}catch(IllegalArgumentException ex) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Rendimento não encontrado");
		}
	}
	
}
