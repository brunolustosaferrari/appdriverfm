package br.com.bruno.appdriverfmapi.rendimentoDiario;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.*;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

@Component
public class RendimentoDiarioResourceAssembler implements ResourceAssembler<RendimentoDiario, Resource<RendimentoDiario>> {
	
	public Resource<RendimentoDiario> toResource(RendimentoDiario rendimento) {
		return new Resource<>(rendimento, linkTo(methodOn(RendimentoDiarioController.class).buscarPorId(rendimento.getIdRendimentoDiario())).withSelfRel());
	}; 
}
