package br.com.bruno.appdriverfmapi.rendimentoDiario;


import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import br.com.bruno.appdriverfmapi.aplicativo.Aplicativo;
import br.com.bruno.appdriverfmapi.diaria.Diaria;
import br.com.bruno.appdriverfmapi.faturamento.Faturamento;
import br.com.bruno.appdriverfmapi.veiculo.Veiculo;

/**
 * Classe responsável por modelar os rendimentos de uma diária em um par aplicativo / veículo. Define na anotação o mapeamento para geração do relatório de faturamento
 *
 */

@SqlResultSetMapping(
		name="FaturamentoResult", 
		classes= {
				@ConstructorResult(
						targetClass=Faturamento.class,
						columns= {
								@ColumnResult(name="idAplicativo", type=Long.class),
								@ColumnResult(name="nomeAplicativo", type=String.class),
								@ColumnResult(name="descricaoAplicativo", type=String.class),
								@ColumnResult(name="distanciaPercorrida", type=float.class),
								@ColumnResult(name="tempoEmCorrida", type=String.class),
								@ColumnResult(name="valor", type=BigDecimal.class),
								@ColumnResult(name="periodo", type=String.class)
						})
		})

@Entity
@Table(name="rendimentoDiario")
public class RendimentoDiario {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long idRendimentoDiario;
	
	@ManyToOne
	@JoinColumn(name="idDiaria")
	@JsonIgnore
	private Diaria diaria;
	
	@ManyToOne
	@JoinColumn(name="idVeiculo")
	@NotNull
	private Veiculo veiculo;
	
	@ManyToOne
	@JoinColumn(name="idAplicativo")
	@NotNull
	private Aplicativo aplicativo;
	
	private float distanciaPercorridaEmKm;
	
	@Temporal(TemporalType.TIME)
	@DateTimeFormat(iso=ISO.DATE_TIME)
	@JsonSerialize(as=Date.class)
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm",timezone="UTC",locale="en_GB")
	private Date tempoEmCorrida;
	
	@NotNull
	private BigDecimal valorRecebido;
	
	public Long getIdRendimentoDiario() {
		return this.idRendimentoDiario;
	}
	
	public RendimentoDiario setIdRendimentoDiario(Long idRendimentoDiario) {
		this.idRendimentoDiario = idRendimentoDiario;
		return this;
	}
	
	public Veiculo getVeiculo() {
		return veiculo;
	}
	
	public RendimentoDiario setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
		return this;
	}
	
	public Aplicativo getAplicativo() {
		return aplicativo;
	}
	
	public RendimentoDiario setAplicativo(Aplicativo aplicativo) {
		this.aplicativo = aplicativo;
		return this;
	}
	
	public float getDistanciaPercorridaEmKm() {
		return distanciaPercorridaEmKm;
	}
	public RendimentoDiario setDistanciaPercorridaEmKm(float distanciaPercorridaEmKm) {
		this.distanciaPercorridaEmKm = distanciaPercorridaEmKm;
		return this;
	}
	
	public Date getTempoEmCorrida() {
		return tempoEmCorrida;
	}
	public RendimentoDiario setTempoEmCorrida(Date tempoEmCorrida) {
		this.tempoEmCorrida = tempoEmCorrida;
		return this;
	}
	
	public BigDecimal getValorRecebido() {
		return valorRecebido;
	}
	
	public RendimentoDiario setValorRecebido(BigDecimal valorRecebido) {
		this.valorRecebido = valorRecebido;
		return this;
	}
	
	public Diaria getDiaria() {
		return this.diaria;
	}
	
	public RendimentoDiario setDiaria(Diaria diaria) {
		this.diaria = diaria;
		return this;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof RendimentoDiario) {
			if(((RendimentoDiario) obj).getIdRendimentoDiario() == this.idRendimentoDiario) {
				return true;
			}
		}
		return false;
	}
}
