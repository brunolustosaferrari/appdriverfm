-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema appdriverfm_db
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema appdriverfm_db
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `appdriverfm_db` DEFAULT CHARACTER SET utf8 ;
USE `appdriverfm_db` ;

-- -----------------------------------------------------
-- Table `appdriverfm_db`.`usuario`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `appdriverfm_db`.`usuario` (
  `idUsuario` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(320) NOT NULL,
  `senha` VARCHAR(64) NOT NULL,
  `autoridade` VARCHAR(10) NULL,
  PRIMARY KEY (`idUsuario`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
